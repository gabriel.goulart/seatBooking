const util = require("util");

var passport = require('passport');
var config = require('./auth');
var userService = require('../services/userService');
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;

var jwtOptions = {
	jwtFromRequest : ExtractJwt.fromAuthHeader(), 
	secretOrKey: config.secret
};

class AuthUser {
	constructor() {
		passport.use(
			new JwtStrategy(
				jwtOptions,
				this._cbInitStrategy
			)
		);
	};

	_cbInitStrategy(payload, done) {
		userService.getUser(payload.idufsc,function(err,row){
			
			if(err){
				return done(err);
			}

			if(!row) {
				return done(null,false);
			}

			return done(null,row);
		});
	};

	isAuthenticated() {
		return passport.authenticate(
			"jwt", 
			{session: false}
		);
	}
};

module.exports = new AuthUser();

/*
module.exports = function(passport)
{


var jwtLogin = new JwtStrategy(jwtOptions,function(payload,done){
	console.log("chegou");
	userService.getUser(payload.idufsc,function(err,row){
		
		if(err){
			return done(err,false);
		}

		if(row){
			 return done(null,row);
		}else{
			 return done(null,false);
		}
	})
});
}
*/
/*
var jwtOptions = {};
    jwtOptions.jwtFromRequest= ExtractJwt.fromAuthHeader();
    jwtOptions.secretOrKey= config.secret;


var jwtLogin = new JwtStrategy(jwtOptions,function(payload,done){
	console.log("chegou");
	userService.getUser(payload.idufsc,function(err,row){
		
		if(err){
			return done(err,false);
		}

		if(row){
			 return done(null,row);
		}else{
			 return done(null,false);
		}
	})
});

passport.use('jwt',jwtLogin); */

