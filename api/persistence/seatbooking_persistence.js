var db = require('./mysql_connection');

exports.createBooking = function(bookingInfo,callback)
{
	return db.query("insert into Reserva (data,idusuario,idviagem,idassento,idreserva_periodica) values(str_to_date(?,'%d-%m-%Y'),?,?,?,?)",[bookingInfo.data,bookingInfo.idusuario,bookingInfo.idviagem,bookingInfo.idassento,bookingInfo.idreservaperiodica],callback);

}

exports.createPeriodicBooking = function(bookingInfo,callback)
{
	return db.query("insert into Reserva_periodica (data_inicio,data_fim,idusuario,idviagem) values(str_to_date(?,'%d-%m-%Y'),str_to_date(?,'%d-%m-%Y'),?,?)",[bookingInfo.datainicial,bookingInfo.datafinal,bookingInfo.idusuario,bookingInfo.idviagem],callback);

}

exports.getBooking = function(bookingInfo,callback)
{
	return db.query("select * from Reserva where data like str_to_date(?,'%d-%m-%Y') and idusuario = ? and idviagem = ?",[bookingInfo.data,bookingInfo.idusuario,bookingInfo.idviagem],callback);
}

exports.getBookingByInfo = function(bookingInfo,callback)
{
	return db.query("select * from Reserva where data like str_to_date(?,'%d-%m-%Y') and idassento = ? and idviagem = ?",[bookingInfo.data,bookingInfo.idassento,bookingInfo.idviagem],callback);
}

exports.getBookings = function(userId,actualDate,callback)
{
	return db.query("select r.idusuario,r.idviagem,date_format(r.data,'%d-%m-%Y') data,r.idassento,r.idreserva_periodica, a.identificacao assento_identificacao, a.preferencial assento_preferencial, l.nome viagem_linha ,h_partida.valor horario_partida, h_chegada.valor horario_chegada from Reserva r inner join Assento a on a.idassento = r.idassento inner join Viagem v on v.idviagem = r.idviagem inner join Linha l on l.idlinha = v.idlinha inner join Horario h_partida on h_partida.idhorario = v.idhorario_partida inner join Horario h_chegada on h_chegada.idhorario = v.idhorario_chegada where isnull(r.idreserva_periodica) and  r.idusuario = ? and r.data >= ? order by r.data",[userId,actualDate],callback);
}

exports.getBookingsByPeriodicId = function(periodicBookingId,callback)
{
	return db.query("select date_format(r.data,'%d-%m-%Y') data,r.idusuario,r.idviagem,r.idassento,r.idreserva_periodica, a.identificacao assento_identificacao,a.preferencial assento_preferencial, l.nome viagem_linha, h_partida.valor horario_partida, h_chegada.valor horario_chegada from Reserva r  inner join Assento a on a.idassento = r.idassento inner join Viagem v on v.idviagem = r.idviagem inner join Linha l on l.idlinha = v.idlinha inner join Horario h_partida on h_partida.idhorario = v.idhorario_partida inner join Horario h_chegada on h_chegada.idhorario = v.idhorario_chegada where r.idreserva_periodica = ?",periodicBookingId,callback);
}

exports.getPeriodicBookings = function(userId,actualDate,callback)
{
	return db.query("select r.idreserva_periodica,date_format(r.data_inicio,'%d-%m-%Y') data_inicio , date_format(r.data_fim,'%d-%m-%Y') data_fim,r.idusuario,r.idviagem,l.nome viagem_linha, h_partida.valor horario_partida, h_chegada.valor horario_chegada from Reserva_periodica r inner join Viagem v on v.idviagem = r.idviagem inner join Linha l on l.idlinha = v.idlinha inner join Horario h_partida on h_partida.idhorario = v.idhorario_partida inner join Horario h_chegada on h_chegada.idhorario = v.idhorario_chegada where r.idusuario = ? and r.data_fim >= ? order by r.data_inicio",[userId,actualDate],callback);
}

exports.deletePeriodicBooking = function(periodicBookingId,callback)
{
	return db.query("delete from Reserva_periodica where idreserva_periodica = ?",periodicBookingId,callback);
}

exports.deleteBookingByPeriodicId = function(periodicBookingId,callback)
{
	return db.query("delete from Reserva where idreserva_periodica = ?",periodicBookingId,callback);
}

exports.deleteBooking=function(bookingDate,bookingTravel,userId,callback)
{
	return db.query("delete from Reserva where data like str_to_date(?,'%d-%m-%Y') and idviagem = ? and idusuario = ?",[bookingDate,bookingTravel,userId],callback);
}

exports.updateBooking=function(bookingInfo,callback)
{
	return db.query("update Reserva set idassento = ? where data like str_to_date(?,'%d-%m-%Y') and idviagem = ? and idusuario = ? ",[bookingInfo.idassento,bookingInfo.data,bookingInfo.idviagem,bookingInfo.idusuario],callback);
}

exports.getCity = function(cityName,callback)
{
	return db.query("select * from Cidade where nome like ?",cityName,callback);
}

exports.getCities = function(callback)
{
	return db.query("select * from Cidade",callback);
}

exports.getBusLines = function(cityId,callback)
{
	return db.query("select * from Linha where idcidade = ?",cityId,callback);
}

exports.getTravels = function(lineId,callback)
{
	return db.query("select v.* ,h1.idhorario as id_horario_viagem_chegada,h1.valor as valor_horario_viagem_chegada,h2.idhorario as id_horario_viagem_partida,h2.valor as valor_horario_viagem_partida, l.nome as nomelinha  from Viagem v inner join Horario h1 on h1.idhorario = v.idhorario_chegada inner join Horario h2 on h2.idhorario = v.idhorario_partida inner join Linha l on l.idlinha = v.idlinha Where v.idlinha = ?",lineId,callback);
}

exports.getTravel = function(travelId,callback)
{
	return db.query("select v.* , Count(ae.idassento) as quantidade_assentos,h1.idhorario as id_horario_viagem_chegada,h1.valor as valor_horario_viagem_chegada,h2.idhorario as id_horario_viagem_partida,h2.valor as valor_horario_viagem_partida, m.nome as motorista_nome, o.modelo as onibus_modelo, o.marca as onibus_marca, l.nome as nomelinha  from Viagem v inner join Horario h1 on h1.idhorario = v.idhorario_chegada inner join Horario h2 on h2.idhorario = v.idhorario_partida inner join Motorista m on v.idmotorista = m.cpf inner join Onibus o on o.idonibus = v.idonibus inner join Linha l on l.idlinha = v.idlinha inner join Assento ae on ae.idonibus = v.idonibus Where v.idviagem = ?",travelId,callback);
}

exports.getBusSeats=function(busId,date,callback)
{
	return db.query("select a.idassento,a.identificacao,a.preferencial,r.idviagem as reserva_viagem, r.data as reserva_data from Assento a left join Reserva r on r.idassento = a.idassento and r.data like str_to_date(?,'%d-%m-%Y') where a.idonibus = ?",[date,busId],callback);
}


exports.getBusStops = function(travelId,callback)
{
	return db.query("select vp.*,p.local as endereco from Viagem_parada vp inner join Parada p on p.idparada = vp.idparada  where idviagem_parada = ?",travelId,callback);
}