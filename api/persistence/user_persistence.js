var db = require('./mysql_connection');

exports.create = function(userInfo,callback)
{

    return db.query("insert into Usuario(idufsc,email,nome) values(?,?,?)",[userInfo.idufsc,userInfo.email,userInfo.nome],callback);
}

exports.getUser = function(userId,callback)
{
	return db.query("select u.*, uc.idcidade,uc.idusuario_cidade , c.nome as nomecidade from Usuario u left join Usuario_cidade uc on u.idusuario = uc.idusuario left join Cidade c on uc.idcidade = c.idcidade  where u.idufsc = ?",userId,callback);
}

exports.getAllUsers = function(callback)
{
	return db.query("select * from Usuario",callback);
}

exports.getCity = function(userId,callback)
{
	return db.query("select * from Cidade c, Usuario u, Usuario_cidade uc where uc.idusuario = u.idusuario and uc.idcidade = c.idcidade and u.idusuario = ?",userId,callback);
}

exports.setCity = function(userCityInfo,update,callback)
{
	if(update)
	{
		console.log("update");
		return db.query("update Usuario_cidade set idcidade = ? where idusuario = ?",[userCityInfo.cityid,userCityInfo.userid],callback);
	}else{
		console.log("insert");
		return db.query("insert into Usuario_cidade (idcidade,idusuario) VALUES(?,?)",[userCityInfo.cityid,userCityInfo.userid],callback);
	}
}

