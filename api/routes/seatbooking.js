var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

var seatBookingService = require('../services/seatBookingService');
const AuthUser = require("../config/passport");

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

/* GET user location. */
router.get('/location/:lat/:long',AuthUser.isAuthenticated(), function(req, res) {
	//res.send('lista de assentos reservados para o usuario : ' + req.params.userId);
	console.log(req.params.lat);
	console.log(req.params.long);
    var lat ='-27.5938346'; //req.params.lat; //'-27.5938346';// //'37.4219983';
    var long ='-48.5407765';//req.params.long;//'-48.5407765';//req.params.long; //'-122.084';
	seatBookingService.getLocation(lat,long,function(body,error){

		    if(!error)
		    {
		    	var info = JSON.parse(body);
		    	console.log(info.results[0].address_components[3].long_name);
		    	seatBookingService.getCity(info.results[0].address_components[3].long_name,function(err,row){

		    		if(err)
		    		{
		    			res.json({'success':false})
		    		}else{
   						console.log(row);
		    			res.json({'success':true,'city':row});
		    		}

		    	});
			    
		    }else{
		    	res.json({'success':false})
		    }
		    
		
	});
	
	
});

router.get('/cities',AuthUser.isAuthenticated(),function(req,res){

	seatBookingService.getCities(function(err,row){
			console.log(row);
			if(err)
			{
				res.json({'success':false});
			}else{
				res.json({'success':true,'cities':row});
			}

	});
	

});

router.get('/busLines/:cityId',AuthUser.isAuthenticated(),function(req,res){
	seatBookingService.getBusLines(req.params.cityId,function(err,row){
			console.log(row);
			if(err)
			{
				res.json({'success':false});
			}else{
				res.json({'success':true,'lines':row});
			}

	});
});

router.get('/travels/:lineId',AuthUser.isAuthenticated(),function(req,res){
	seatBookingService.getTravels(req.params.lineId,function(err,row){
			console.log(row);
			if(err)
			{
				res.json({'success':false});
			}else{
				res.json({'success':true,'travels':row});
			}

	});
});

router.get('/travel/:travelId/:dateinfo',AuthUser.isAuthenticated(),function(req,res){
	seatBookingService.getTravel(req.params.travelId,function(err,row){
			console.log(row[0].idonibus);
			if(err)
			{
				res.json({'success':false});
			}else{

				seatBookingService.getBusSeats(row[0].idonibus,req.params.dateinfo,function(err,row2){
					if(err)
					{
						console.log(err);
						res.json({'success':false});
					}else{

						seatBookingService.getBusStops(req.params.travelId,function(err,row3){

							if(err)
							{
								res.json({'success':false});
							}else{
								res.json({'success':true,'travel_info': row, 'bus_seats' : row2, 'bus_stops' : row3});
							}
						});
						
					}
					
				});
				
			}

	});
});


/* create a new seat booking */
router.post('/booking',AuthUser.isAuthenticated(), function(req, res) {
	//console.log('chegou aqui : ' + req.body.nome + '-'  + req.body.idade ); //[req.body.nome,req.body.idade]
	seatBookingService.getBooking(req.body,function(err,row){
		if(err)
		{	
			res.json({'success':false});
		}else if(row.length > 0){
			res.json({'success':false, 'message' : 'Você já tem uma reserva para essa viagem'});
		}else{

			seatBookingService.createBooking(req.body,function(err,rows){

				if(err)
				{
					res.json({'success':false});
				}else
				{
					res.json({'success': true});
				}
			}); 
		}
	});
	
});


/* create a new periodic seat booking */
router.post('/periodicBooking', AuthUser.isAuthenticated(),function(req, res) {
	//console.log('chegou aqui : ' + req.body.nome + '-'  + req.body.idade ); //[req.body.nome,req.body.idade]
	seatBookingService.createPeriodicBooking(req.body,function(err,row){
		if(err)
		{	
			res.json({'success':false,'message' : err});
		}else {

			res.json({'success':true});
		}
	});
	
  	//res.send('respond with a resource');
});

router.delete('/periodicBooking/:bookingId',AuthUser.isAuthenticated(),function(req,res){
	console.log(req.params.bookingId );
	
	seatBookingService.deletePeriodicBooking(req.params.bookingId,function(err,row){
			
			if(err)
			{
				res.json({'success':false});
			}else{
				res.json({'success':true});
					
			}

	});
});

router.get('/allBookings/:userId',AuthUser.isAuthenticated(),function(req,res){
	seatBookingService.getBookings(req.params.userId,function(err1,row1){
			
			if(err1)
			{
				res.json({'success':false});
			}else{
				seatBookingService.getPeriodicBookings(req.params.userId,function(err2,row2){
					
					if(err2)
					{
						res.json({'success':false});
					}else{
						res.json({'success':true,'single':row1,'periodic':row2});
					}
				});
			}

	});
});

router.delete('/booking/:bookingDate/:bookingTravel/:userId',AuthUser.isAuthenticated(),function(req,res){
	console.log(req.params.bookingDate + " - " +req.params.bookingTravel + " - " + req.params.userId );
	
	seatBookingService.deleteBooking(req.params.bookingDate,req.params.bookingTravel,req.params.userId,function(err,row){
			
			if(err)
			{
				res.json({'success':false});
			}else{
				res.json({'success':true});
					
			}

	});
});

router.put('/booking',AuthUser.isAuthenticated(),function(req,res){
	console.log(req.body.data + " - " +req.body.idassento + " - " + req.body.idusuario  + "-"+ req.body.idviagem );
	
	seatBookingService.updateBooking(req.body,function(err,row){
			
			if(err)
			{
				res.json({'success':false});
			}else{
				res.json({'success':true});
					
			}

	});
});


router.get('/bookingsByPeriodicId/:periodicBookingId',AuthUser.isAuthenticated(),function(req,res){
	seatBookingService.getBookingsByPeriodicId(req.params.periodicBookingId,function(err,row){
			
			if(err)
			{
				res.json({'success':false});
			}else{
				
				res.json({'success':true,'bookings':row});
				
			}

	});
});

module.exports = router;
