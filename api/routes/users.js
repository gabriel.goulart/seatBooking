var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var passport = require('passport');

var userService = require('../services/userService');
var authenticationService = require('../services/authenticationService');
// var requireAuth = passport.authenticate('jwt',{session:false});
const AuthUser = require("../config/passport");


router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

/* GET users listing. */
router.get("/", AuthUser.isAuthenticated() , (req, res) => {
	res.status(200).json({
		msg: "oK"
	});
})

/*
router.get('/',requireAuth, function(req, res) {
	//db.query("select u.nome, c.marca from user_car uc right join user_teste u on u.id = uc.userid left join carro c on c.id = uc.carroid", function(err,rows){
	
	
  //res.send('respond with a resource');
  userService.getAllUsers(function(err,rows){

		if(err)
		{
			res.json(err);
		}else
		{
			res.status(200).json(rows);
		}
  }); 

  
});
/*

/* create a new user */
router.post('/create', function(req, res) {
	//console.log('chegou aqui : ' + req.body.nome + '-'  + req.body.idade ); //[req.body.nome,req.body.idade]
	/*db.query("insert into usuario (nome,idade) values (?,?)",[req.body.nome,req.body.idade],function(err,rows){

		if(err)
		{
			res.json(err);
		}else
		{
			res.json('{success: true}');
		}
	}); */
  	//res.send('respond with a resource');

  	userService.createUser(req.body,function(err,rows){
  		if(err)
		{
			res.json(err);
		}else
		{
			res.json("success: true");
		}
  	});
});

router.post('/city',AuthUser.isAuthenticated(),function(req, res) {
	console.log(req.body.userid);
  	userService.getUserCity(req.body.userid,function(err,row){
  		var update;
  		if(err)
		{
			res.json({success:false});
		}else
		{
			update = true;
			console.log("QUANTIDAE LINHA " + row.length)
			if(row.length === 0)
			{
				update = false;
			}
			userService.setUserCity(req.body,update,function(err,rows){
		  		if(err)
				{
					console.log("dado false : " + update);
					res.json({"success":false});
				}else
				{
					console.log("dado true : " + update);
					res.json({"success": true, 'message' : "worked"});
				
			     }
  			});
  		}	
	});

});

/* return the url to user complete the authentication using the idUfsc */
router.get('/authenticate',function(req,res){
	res.json({"success":true, "url":"https://projetopedagogico.inf.ufsc.br/ine542420171/"});
});

router.post('/register',authenticationService.register);

module.exports = router;
