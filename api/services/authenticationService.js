var jwt = require('jsonwebtoken');  
var userService = require('./userService');
var authConfig = require('../config/auth');
 
function generateToken(id){
    return jwt.sign(id, authConfig.secret);
}

//, {
 //       expiresIn: 10080
//    }
function setUserInfo(request){
    return {
        idUfsc: request.idufsc,
        email: request.email,
        nome: request.nome
    };
}
exports.register = function(req, res, next){
 
    var idUfsc = req.body.idufsc;
    var nome = req.body.nome;
    var email = req.body.email;

    if(!idUfsc){
        return res.status(422).send({error: 'Problema na autenticação (IdUFSC)'});
    }
 
    if(!nome){
        return res.status(422).send({error: 'Problema na autenticação (Nome)'});
    }

    userService.getUser(idUfsc,function(err,row){
          
    	if(err)
    	{
    		return res.status(422).send({error: 'Problema na autenticação'});
    	}else{
    		
    		if(row.length === 0)
    		{
                // user doesnt exist in database
    			userService.createUser(req.body,function(err,row){

    				if(err)
    				{
    					return res.status(422).send({error: 'Problema na autenticação'});
    				}else{
                        
    					res.status(200).json({
					    	success: true,
                            userid:row.insertId,
                            cityname: null,
                            cityid: null,
                            usercityid: null,
					        token: "JWT "  + generateToken(req.body.idufsc)        
					    });
    				}

    			});	
    			

    		}else{
                // user already exists in database
               console.log(row);
    			res.status(200).json({
			    	success: true,
                    userid:row[0].idusuario,
                    cityname: row[0].nomecidade,
                    cityid: row[0].idcidade,
                    usercityid: row[0].idusuario_cidade,
			        token: 'JWT ' + generateToken(req.body)        
			    });
    			
    		}
    	}

    });
 
    
 
}