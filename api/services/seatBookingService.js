var seatBooking_persistence = require('../persistence/seatBooking_persistence');
var http = require('https');
var request = require('request');
var moment = require('moment');
moment.locale('pt-br');

exports.getLocation=function(lat,long,callback)
{
	
		 request('https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+long,{timeout:1500},function(erro,response,body){
				//console.log(JSON.parse(body));
				//var info = JSON.parse(body);
				//console.log(info.results[0].address_components[3].long_name);
				return callback(body,erro);
				//return  getCity(info.results[0].address_components[3].long_name,callback);
		}); 

}

exports.createBooking=function(bookingInfo,callback)
{
	return seatBooking_persistence.createBooking(bookingInfo,callback);
}


exports.createPeriodicBooking=function(bookingInfo,callback)
{
	funcRetorno = callback;
	seatBooking_persistence.createPeriodicBooking(bookingInfo, function(err,rowcreatperiodicbooking){

		if(err){
			
			callback(err,null);
		}else{
				var initialDate = moment (bookingInfo.datainicial,'DD-MM-YYYY');//new Date("2017-03-19");
				var finalDate = moment (bookingInfo.datafinal,'DD-MM-YYYY');//new Date("2017-03-21");
				
				//var data = initialDate.format('DD-MM-YYYY') + " ** " + finalDate.format('DD-MM-YYYY');
				//console.log("DIA DA SEMANA = " + initialDate.weekday());
				
				var weekday = bookingInfo.diasemana;
				weekday = weekday.split(",");
				console.log(weekday[0]);

				if(weekday[0] == "true")
				{
					var weekdaySelected = [1, 2, 3,4, 5];
				}else{
					var weekdaySelected = [-1, -1, -1,-1, -1];
					for(i=1;i< weekday.length;i++)
					{
						if(weekday[i] == "true")
						{
							weekdaySelected[i-1] = i;
						}


					}	
				}
					
				fim = false; 
				
				dates = [];
				if(weekdaySelected.includes(initialDate.weekday()))
				{
					
					dates.push(initialDate.format('DD-MM-YYYY'));
					
				}
				y=1;
				while(!fim)
				{
					
					data_temp2="";
					data_temp= "";
					data_temp2 = initialDate;
					data_temp = data_temp2.add(1,'d');
					
					if(data_temp > finalDate)
					{
						fim = true;
					}else{
						y++;
						if(weekdaySelected.includes(data_temp.weekday()))
						{

							dates.push(data_temp.format('DD-MM-YYYY'));	
						}
						
					}
				}

				
				
				// global controll, it is used to return the information when everything is done
				control=0;
				booking_error = [];
				

				for(i=0;i < dates.length ; i++)
				{
					//console.log("data : "+dates[i]);
					
					var info = {
								data: dates[i],
								idusuario: bookingInfo.idusuario,
								idviagem: bookingInfo.idviagem,
								idassento: bookingInfo.idassento,
								idreservaperiodica: rowcreatperiodicbooking.insertId
							} ; 
							
							
						booking(info);
				} 

			}

	});

}

function booking(info)
{
	seatBooking_persistence.getBookingByInfo(info,function(err1,row1){

		if(err1)
		{
				booking_error.push({
					message: "error",
					date:info.data
				});
				control++;
				if(control >= dates.length ){
					if(booking_error.length > 0)
					{
						funcRetorno(booking_error,null);
					}else{
						funcRetorno(null,true);
					}
									
				}else{return true;} 

		}else{
			//console.log("ROW : " + row1.length);
			if(row1.length == 0 )
			{
				seatBooking_persistence.createBooking(info,function(err,row){
								
								if(err)
								{
									
									booking_error.push({
										message: "error",
										date:info.data
									});  

								}

								control++;
								console.log(control);
								if(control >= dates.length ){
									if(booking_error.length > 0)
									{
										funcRetorno(booking_error,null);
									}else{
										funcRetorno(null,true);
									}
									
								}else{return true;} 
							});
			}else{
				booking_error.push({
					message: "error",
					date:info.data
				}); 
				control++;
				if(control >= dates.length ){
					if(booking_error.length > 0)
					{
						funcRetorno(booking_error,null);
					}else{
						funcRetorno(null,true);
					}
					
				}else{return true;} 
			}
		}
		
	});
	
}

exports.deletePeriodicBooking=function(bookingId,callback)
{
	seatBooking_persistence.deleteBookingByPeriodicId(bookingId,function(err1,row){
		if(err1)
		{
			callback(true,null);
		}else{
			seatBooking_persistence.deletePeriodicBooking(bookingId,function(err2,row){
				if(err2)
				{
					callback(true,null);
				}else{
					callback(false,null);
				}
			});
		}
	});
}

exports.getBooking=function(bookingInfo,callback)
{
	return seatBooking_persistence.getBooking(bookingInfo,callback);
}


exports.getCity = function(cityName,callback)
{
	return seatBooking_persistence.getCity(cityName,callback);
}

exports.getCities=function(callback)
{
	return seatBooking_persistence.getCities(callback);
}

exports.getBusLines=function(cityId, callback)
{
	return seatBooking_persistence.getBusLines(cityId,callback);
}

exports.getTravels=function(lineId,callback)
{
	return seatBooking_persistence.getTravels(lineId,callback);
}

exports.getTravel=function(travelId,callback)
{

	return seatBooking_persistence.getTravel(travelId,callback);	
	
}

exports.getBusSeats=function(busId,date,callback)
{
	return seatBooking_persistence.getBusSeats(busId,date,callback);
}

function actualDate()
{
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth()+1;
	if(month < 10 )
	{
		month = "0"+ month;
	}

	var day = date.getDate();
	if(day < 10 )
	{
		day = "0"+ day;
	}
	
	date = year + "-" + month + "-" + day;
	console.log(date);
	return date;
}
exports.getBookings=function(userId,callback){
	var date= actualDate();
	return seatBooking_persistence.getBookings(userId,date,callback);
}

exports.getPeriodicBookings=function(userId,callback){
	var date= actualDate();
	return seatBooking_persistence.getPeriodicBookings(userId,date,callback);
}

exports.getBookingsByPeriodicId = function(periodicBookingId,callback)
{
	return seatBooking_persistence.getBookingsByPeriodicId(periodicBookingId,callback);
}

exports.deleteBooking=function(bookingDate,bookingTravel,userId,callback){
	return seatBooking_persistence.deleteBooking(bookingDate,bookingTravel,userId,callback);
}

exports.updateBooking=function(bookingInfo,callback){
	return seatBooking_persistence.updateBooking(bookingInfo,callback);
}
exports.getBusStops=function(travelId,callback){
	return seatBooking_persistence.getBusStops(travelId,callback);
}

