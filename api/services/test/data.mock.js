"use strict";

const MOCK_DATA = {
	algorithm: "HS256",
	expiresIn: "10y",
	secret: "13412433@#$%ˆ%ˆ%FFTD$AA%$AS%$AS%A#A%A$*HA*&DGB1354",
	token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZHVmc2MiOiJnYWJyaWVsLmdvdWxhcnQyIiwibm9tZSI6ImdhYnJpZWwgZ291bGFydDIiLCJlbWFpbCI6ImdhYnJpZWwtci1nMkBob3RtYWlsLmNvbSIsImlhdCI6MTQ5NzczNDAzOCwiZXhwIjoxODEzMzEwMDM4fQ.4tPXT9a1exnzxaUnuWlqalJLI4hbD9g-imTto50sl2g",
	payload: {
		idufsc: 'gabriel.goulart2',
     	nome: 'gabriel goulart2',
     	email: 'gabriel-r-g2@hotmail.com' 
	}
};

module.exports = MOCK_DATA;