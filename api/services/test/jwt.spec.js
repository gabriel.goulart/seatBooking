"use strict";

const util = require("util");
const chai = require("chai");
const expect = chai.expect;
const should = chai.should();

// MOCK DATA 
const MOCK_DATA = require("./data.mock");

// LIVRARIAS PARA O SERVICO MOCKADO 
const jwt = require("jsonwebtoken");

// MOCKANDO O SERVICO JWT SERVICE 
const JwtService = () => {
	let _encode = payload => {
		return new Promise((resolve, reject) => {
			jwt.sign(
				payload, 
				MOCK_DATA.secret, 
				{
					algorithm: MOCK_DATA.algorithm, 
					expiresIn: MOCK_DATA.expiresIn
				}, 
				(err, token) => {
					if(err) {
						return reject(err);
					}

					return resolve(token);
				}
			);
		});
	};

	let _validate = token => {
		return new Promise((resolve, reject) => {
			jwt.verify(
				token, 
				MOCK_DATA.secret, 
				{
					algorithms: [ MOCK_DATA.algorithm ]
				}, 
				(err, payload) => {
					if(err) {
						return reject(err);
					}

					return resolve(payload);
				}
			)
		});
	};

	let _decode = token => {
		return jwt.decode(
			token, 
			{
				complete: true
			}
		);
	};

	return {
		decode: _decode, 
		encode: _encode,
		validate: _validate
	}	
};

// CENARIOS DE TESTES 
describe("JWT SERVICE", () => {
	let jwtService = null;

	before(done => {
		jwtService = JwtService();
		done();
	});

	it("should decode informed token", done => {
		// util.log(MOCK_DATA);
		let jwtDecoded = jwtService.decode(MOCK_DATA.token);
		expect(jwtDecoded.payload).to.be.ok;
		expect(jwtDecoded.header.alg).to.be.equal(MOCK_DATA.algorithm);
		expect(jwtDecoded.payload.idufsc).to.be.equal("gabriel.goulart2");
		done();
	});

	it("should encode new token with got payload", done => {
		jwtService.encode(
			MOCK_DATA.payload
		)
		.then(token => {
			expect(token).to.be.ok;
			done();
		})
		.catch(err => {
			console.error(err.message);
			return done(err);
		})
	});

	it("should validate token", done => {
		jwtService.validate(MOCK_DATA.token)
			.then(payload => {
				expect(payload).to.be.ok;			
				expect(payload.idufsc).to.be.equal("gabriel.goulart2");
				done();
			})
			.catch(err => {
				console.error(err.message);
				return done(err)
			});
	});

	after(done => {
		util.log('Teste finalizado');
		done();
	});
});