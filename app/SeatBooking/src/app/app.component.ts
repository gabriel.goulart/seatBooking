import { Component,ViewChild } from '@angular/core';
import { Nav,Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Login} from '../modules/authentication/pages/login/login';
import { Logout} from '../modules/authentication/pages/logout/logout';
import { List_reserves} from '../modules/list_reserves/pages/list_reserves/list_reserves';
import { Booking} from '../modules/booking/pages/booking/booking';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

 // setting the root page to login page
  rootPage:any = Login;

  // array with menu options, which is selected its module is called
  menu_options : Array<{title: string , option : any}>;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.menu_options = [
      {title: "Listar Reservas" , option : List_reserves},
      {title: "Reservar" , option : Booking},
      {title: "Logout" , option : Logout},
    ];
  }

  // switch to the module selected
  openOption(option_selected) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(option_selected.option);
  }
}
