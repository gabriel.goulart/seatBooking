import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import {HttpModule} from '@angular/http';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { DatePicker } from '@ionic-native/date-picker';



import { MyApp } from './app.component';
import { List_reserves_module } from '../modules/list_reserves/list_reserves.module';
import { Booking_module } from '../modules/booking/booking.module';
import { Authentication_module } from '../modules/authentication/authentication.module';

//import the services
import {Authentication} from '../modules/authentication/services/authentication.service';
import {Booking_Service} from '../modules/booking/services/booking.service';
import {List_Reserves_Service} from '../modules/list_reserves/services/list_reserves.service';
import {Http_Service}   from '../services/http.service';
import {Storage_Service} from '../services/persistence/storage.service';
import {UserPersistence_Service} from '../services/persistence/userPersistence.service';
import {Geolocation_Service}   from '../services/geolocation.service';

@NgModule({
  declarations: [
    MyApp

  ],
  imports: [
    BrowserModule,
    List_reserves_module,
    Booking_module,
    Authentication_module,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,

  ],
  providers: [
    DatePicker,
    Geolocation,
    Authentication,
    Booking_Service,
    List_Reserves_Service,
    Geolocation_Service,
    Http_Service,
    Storage_Service,
    UserPersistence_Service,
    InAppBrowser,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
