/* messages from the system */

/*GENERAL MESSAGES */
export const MESSAGE_YES = "sim";
export const MESSAGE_NO = "não";
export const MESSAGE_CANCEL = "Cancelar";
export const MESSAGE_ERROR = "Erro";
export const MESSAGE_REFRESH_PULL_TEXT = "Atualizar";
export const MESSAGE_REFRESH_REFRESHING_TEXT = "Atualizando";
export const MESSAGE_VERIFY_TEXT = "Verificando"

/* PAGES TITLES */
export const TITLE_PAGE_BOOKING_SINGLE_BOOKING = "Reserva Única";
export const TITLE_PAGE_BOOKING_PERIODIC_BOOKING = "Reserva Periódica";
export const TITLE_PAGE_BOOKING_SELECT_DATE_TIME = "Selecionar Data e Horário";
export const TITLE_PAGE_LIST_BOOKINGS = "Lista de Reservas";
export const TITLE_PAGE_LIST_BOOKINGS_FROM_PERIODIC = "Lista de Reservas";


/* LOGIN MESSAGES */
export const MESSAGE_AUTHENTICATION_DO_LOGIN = "Realizando Login" ;
export const MESSAGE_AUTHENTICATION_DO_LOGIN_ERROR_TITLE = "Problema ao realizar o login";
export const MESSAGE_AUTHENTICATION_DO_LOGIN_ERRO_TEXT = "Favor tentar novamente";

/* BOOKING MESSAGES */
export const MESSAGE_BOOKING_SEARCHING_LOCATION = "Determinando a sua localização";
export const MESSAGE_BOOKING_SEARCHING_LOCATION_ERROR = "Infelizmente não conseguimos determinar a sua localização";
export const MESSAGE_BOOKING_SEARCHING_LOCATION_USE_CURRENT_CITY = "Usar a sua cidade?";
export const MESSAGE_BOOKING_NO_CITY = "Nenhuma cidade foi encontrada";
export const MESSAGE_BOOKING_UPDATING_CITY = "Atualizando";
export const MESSAGE_BOOKING_NO_LINES = "Nenhuma linha foi encontrada";
export const MESSAGE_BOOKING_SEARCHING_LINES = "Verificando a lista de linhas";
export const MESSAGE_BOOKING_LINE_ACTION_INFO = "Informações";
export const MESSAGE_BOOKING_LINE_ACTION_BOOKING = "Reserva Única";
export const MESSAGE_BOOKING_LINE_ACTION_BOOKING_PERIODIC = "Reserva Periódica";
export const MESSAGE_BOOKING_LINE_ACTION_CANCEL = "Cancelar";
export const MESSAGE_BOOKING_GETTING_TRAVELS = "Verificando";
export const MESSAGE_BOOKING_GETTING_TRAVEL_INFO = "Colhendo as informações";
export const MESSAGE_BOOKING_INFO_ERROR = "Infelizmente não conseguimos colher as informações";
export const MESSAGE_BOOKING_SELECT_DATE_AGAIN = "Selecionar Data novamente";
export const TEXT_BOOKING_SELECT_DATE = "Selecionar Data";
export const TEXT_BOOKING_SELECT_WEEK_DAYS = "Selecionar Dias da Semana";
export const TEXT_BOOKING_SELECT_INITIAL_DATE = "Selecionar Data Inicial";
export const TEXT_BOOKING_SELECT_FINAL_DATE = "Selecionar Data Final";
export const TEXT_BOOKING_SELECT_TIME = "Selecionar Horário";
export const TEXT_BOOKING_BUTTON_CHANGE_DATE = "Mudar Data";

export const TEXT_BOOKING_WEEK_DAY_ALL = "Todos os dias";
export const TEXT_BOOKING_WEEK_DAY_MONDAY = "Segunda-Feira";
export const TEXT_BOOKING_WEEK_DAY_TUESDAY = "Terça-Feira";
export const TEXT_BOOKING_WEEK_DAY_WEDNESDAY = "Quarta-Feira";
export const TEXT_BOOKING_WEEK_DAY_THURSDAY = "Quinta-Feira";
export const TEXT_BOOKING_WEEK_DAY_FRIDAY = "Sexta-Feira";

export const TEXT_BOOKING_GENERAL_TRAVEL_INFO = "GERAL";
export const TEXT_BOOKING_GENERAL_TRAVEL_INFO_LINE = "Linha";
export const TEXT_BOOKING_GENERAL_TRAVEL_INFO_DEPARTURE_TIME = "Partida";
export const TEXT_BOOKING_GENERAL_TRAVEL_INFO_ARRIVAL_TIME = "Chegada";

export const TEXT_BOOKING_BUS_TRAVEL_INFO= "ÔNIBUS";
export const TEXT_BOOKING_BUS_TRAVEL_INFO_MODEL = "Modelo";
export const TEXT_BOOKING_BUS_TRAVEL_INFO_BRAND = "Marca";
export const TEXT_BOOKING_BUS_TRAVEL_INFO_AMOUNT_SEATS = "Assentos";
export const TEXT_BOOKING_BUS_TRAVEL_INFO_AMOUNT_SEATS_PREFERENTIAL = "Assentos Preferenciais";
export const TEXT_BOOKING_BUS_TRAVEL_INFO_AMOUNT_SEATS_BOOKED = "Assentos Reservados";

export const TEXT_BOOKING_DRIVER_TRAVEL_INFO= "MOTORISTA";
export const TEXT_BOOKING_DRIVER_TRAVEL_INFO_NAME= "Nome";
export const TEXT_BOOKING_DRIVER_TRAVEL_EMAIL= "E-mail";
export const TEXT_BOOKING_STOP_TRAVEL_INFO= "PARADAS";

export const MESSAGE_BOOKING_SINGLE_PERIODIC_SEAT_SELECTED = "Assento Selecionado";
export const MESSAGE_BOOKING_SINGLE_PERIODIC_DO_BOOKING = "Realizando a reserva";
export const MESSAGE_BOOKING_SINGLE_PERIODIC_DO_BOOKING_SUCCESS = "Reserva realziada";
export const MESSAGE_BOOKING_SINGLE_PERIODIC_EDITING_BOOKING = "Reserva Editada";
export const TEXT_BOOKING_SINGLE_PERIODIC_DRIVER_SEAT = "Motorista";
export const TEXT_BOOKING_SINGLE_PERIODIC_FREE_SEAT = "Livre";
export const TEXT_BOOKING_SINGLE_PERIODIC_BUSY_SEAT = "Ocupado";
export const TEXT_BOOKING_SINGLE_PERIODIC_PREFERENTIAL_SEAT = "Preferencial";
export const TEXT_BOOKING_SINGLE_PERIODIC_SEAT_ALREADY_BOOKED = "Assentos Já Reservados";
export const TEXT_BOOKING_SINGLE_PERIODIC_CHANGE_SEAT = "Mudar Assento";

export const MESSAGE_LIST_BOOKINGS_GET_BOOKINGS = "Verificando as reservas";
export const MESSAGE_LIST_BOOKINGS_DELETING_BOOKING = "Excluindo a reserva";
export const MESSAGE_LIST_BOOKINGS_DELETED_BOOKING = "Reserva excluida";
export const MESSAGE_LIST_BOOKINGS_GETTING_TRAVEL_INFO = "Colhendo as informações";
export const TEXT_LIST_BOOKINGS_SEGMENT_SINGLE = "Única";
export const TEXT_LIST_BOOKINGS_SEGMENT_PERIODIC = "Periódica";
export const TEXT_LIST_BOOKINGS_SEGMENT_SINGLE_DATE = "Data";
export const TEXT_LIST_BOOKINGS_SEGMENT_SINGLE_SEAT = "Assento";
export const TEXT_LIST_BOOKINGS_BUTTON_EDIT = "EDITAR";
export const TEXT_LIST_BOOKINGS_BUTTON_EXCLUDE = "EXCLUIR";
export const TEXT_LIST_BOOKINGS_SEGMENT_PERIODIC_INITIAL_DATE = "Data inicial";
export const TEXT_LIST_BOOKINGS_SEGMENT_PERIODIC_FINAL_DATE = "Data final";
export const TEXT_LIST_BOOKINGS_TRAVEL_INFO_DEPARTURE_TIME = "Partida";
export const TEXT_LIST_BOOKINGS_TRAVEL_INFO_ARRIVAL_TIME = "Chegada";


/* HTTP MESSAGES */
export const MESSAGE_HTTP_ERROR = "Problema na conexão, tente novamente";
