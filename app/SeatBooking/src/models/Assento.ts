export class Assento
{
    id:number;
    preferencial:boolean;
    ocupado:boolean = false;
    nome:string;

    constructor(id,preferencial,nome,ocupado?){
      this.id = id;
      this.preferencial = preferencial;
      this.nome = nome;
      this.ocupado = ocupado;
    }

    get_id()
    {
      return this.id;
    }

    get_preferencial()
    {
      return this.preferencial;
    }

    get_nome()
    {
      return this.nome;
    }

    get_ocupado()
    {
      return this.ocupado;
    }
    set_id(newId)
    {
      this.id = newId;
    }

    set_preferencial(preferencial)
    {
      this.preferencial = preferencial;
    }

    set_nome(newNome)
    {
      this.nome = newNome;
    }

    set_ocupado(newOcupado)
    {
      this.ocupado = newOcupado;
    }
}
