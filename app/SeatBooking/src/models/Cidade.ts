export class Cidade{

  id:number;
  nome:string;

  constructor(id,nome){
    this.id=id;
    this.nome=nome;
  }

  public get_nome()
  {
    return this.nome;
  }

  public get_id()
  {
    return this.id;
  }

  public set_nome(nome)
  {
    this.nome=nome;
  }

  public set_id(id)
  {
    this.id=id;
  }

  public serialize()
  {
    return {
      id:this.get_id(),
      nome:this.get_nome(),
    };
  }
}
