export class Motorista
{
  id:number;
  nome:string;
  email:string;


  constructor(id,nome,email?)
  {
    this.id = id;
    this.nome = nome;
    this.email = email;
  }

  public get_id()
  {
    return this.id;
  }

  public get_nome()
  {
    return this.nome;
  }

  public get_email()
  {
    return this.email;
  }

  public set_id(newId)
  {
    this.id = newId;
  }

  public set_nome(newNome)
  {
    this.nome = newNome;
  }

  public set_email(newEmail)
  {
    this.email = newEmail;
  }
}
