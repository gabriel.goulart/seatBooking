import {Assento} from './Assento';
export class Onibus
{
  id:number;
  modelo:string;
  marca:string;
  assentos=[];


  constructor(id,modelo,marca?,assentos?)
  {
    this.id = id;
    this.modelo = modelo;
    this.marca = marca;
    this.assentos = assentos;
  }

  public get_id()
  {
    return this.id;
  }

  public get_modelo()
  {
    return this.modelo;
  }

  public get_marca()
  {
    return this.marca;
  }

  public get_assentos()
  {
    return this.assentos;
  }

  public quantidade_assentos()
  {
    return this.assentos.length;
  }

  public quantidade_assentos_preferenciais()
  {
    let count = 0;
    for(let assento of this.assentos)
    {

      if(assento.get_preferencial())
      {
        count++;
      }
    }

    return count;
  }

  public quantidade_assentos_ocupados()
  {
    let count = 0;
    for(let assento of this.assentos)
    {

      if(assento.get_ocupado())
      {
        count++;
      }
    }

    return count;
  }
  public set_id(newId)
  {
    this.id = newId;
  }

  public set_modelo(newModelo)
  {
    this.modelo = newModelo;
  }

  public set_marca(newMarca)
  {
    this.marca = newMarca;
  }

  public set_assentos(newAssentos)
  {
    this.assentos = newAssentos;
  }

  public set_assento(assento)
  {
    this.assentos.push(assento);
  }
}
