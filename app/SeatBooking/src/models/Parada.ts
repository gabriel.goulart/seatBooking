export class Parada {
  id: number;
  endereco:string;

  constructor(id,endereco){
    this.id = id;
    this.endereco = endereco;
  }

  public get_id()
  {
    return this.id;
  }

  public get_endereco()
  {
    return this.endereco;
  }

  public set_id(newId)
  {
    this.id = newId;
  }

  public set_endereco(newEndereco)
  {
    this.endereco = newEndereco;
  }
}
