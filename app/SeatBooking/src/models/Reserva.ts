import {Assento} from './Assento';
import {Viagem} from './Viagem';

export abstract class Reserva {
  //data:any;
  assento:Assento;
  viagem:Viagem;
  idreserva_periodica:number = null;

  constructor(viagem,assento,idreserva_periodica?){
    //this.data = data;
    this.viagem = viagem;
    this.assento = assento;
    this.idreserva_periodica = idreserva_periodica;
  }


/*  public get_data()
  {
    return this.data;
  } */

  public get_viagem()
  {
    return this.viagem;
  }

  public get_assento()
  {
    return this.assento;
  }

  public get_idreserva_periodica()
  {
    return this.idreserva_periodica;
  }

  /*public set_data(newData)
  {
    this.data =newData ;
  }*/

  public set_viagem(newViagem)
  {
    this.viagem = newViagem;
  }

  public set_assento(newIdassento)
  {
    this.assento = newIdassento;
  }

  public set_idreserva_periodica(newIdreserva_periodica)
  {
    this.idreserva_periodica = newIdreserva_periodica;
  }

}
