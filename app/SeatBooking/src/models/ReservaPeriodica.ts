import {Reserva} from './Reserva';

export class ReservaPeriodica extends Reserva
{
  reservas : Reserva[] = [];
  data:any = [];
  //id:number;

  constructor(id,data,viagem,reservas?){
    super(viagem,null,id);
    this.data = data;
    this.viagem = viagem;
    this.idreserva_periodica = id;
    this.reservas = reservas;

  }

  public get_id()
  {
    return this.idreserva_periodica;
  }
  public get_reservas()
  {
    return this.reservas;
  }

  public get_data_inicial()
  {
    return this.data[0];
  }

  public get_data_final()
  {
    return this.data[1];
  }

  public set_id(newId)
  {
    this.idreserva_periodica = newId;
  }

  public set_reservas(newReservas)
  {
    this.reservas = newReservas;
  }

  public set_reserva(newReserva)
  {
    this.reservas.push(newReserva);
  }

  public set_data_inicial(newData)
  {
    this.data[0] = newData;
  }

  public set_data_final(newData)
  {
    this.data[1] = newData;
  }

}
