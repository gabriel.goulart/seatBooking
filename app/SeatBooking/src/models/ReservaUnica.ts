import {Reserva} from './Reserva';

export class ReservaUnica extends Reserva
{
  data:string;
  constructor(data,viagem,assento,idreserva_periodica?){
    super(viagem,assento,idreserva_periodica);
    this.data = data;
  }

  public get_data()
  {
    return this.data;
  } 
  public set_data(newData)
  {
    this.data =newData ;
  }

}
