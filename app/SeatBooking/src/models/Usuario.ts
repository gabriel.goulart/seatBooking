import {Cidade} from './Cidade';
import {Reserva} from './Reserva';
export class Usuario
{
  static instance:Usuario;
  static isCreating:Boolean = false;

  private id:number;
  private nome:string;
  private idufsc:string;
  private email:string;
  private token:string = null;
  private cidade:Cidade; // objeto cidade
  private reservasUnica: Reserva[] = [];
  private reservasPeriodica: Reserva[] = [];

  constructor(){
    if(!Usuario.isCreating)
   {
     throw new Error(" Não Pode criar uma outra instância");
   }
  }

 /* Singleton */

  static getInstance()
  {
    if(Usuario.instance == null)
    {
      Usuario.isCreating = true;
      Usuario.instance = new Usuario();
      Usuario.isCreating = false;
      Usuario.getInstance().set_cidade(null);
    }

    return Usuario.instance;
  }

  public delete_user()
  {
    Usuario.instance = null;
  }
  /* set the id*/
  public set_id(new_id)
  {
   this.id = new_id;
  }

  /* set the nome*/
  public set_nome(new_nome)
  {
   this.nome = new_nome;
  }
 /* set the idufsc */
  public set_idufsc(new_idufsc)
  {
    this.idufsc = new_idufsc;
  }

  /* set the email*/
  public set_email(new_email)
  {
   this.email = new_email;
  }

  /* set the token*/
  public set_token(new_token)
  {
   this.token = new_token;
  }

  /* set the city*/
  public set_cidade(new_cidade:Cidade)
  {
   this.cidade = new_cidade;
  }

  /* set the list of single booking */
  public set_reservas_unica(newReservasUnica)
  {
    this.reservasUnica = newReservasUnica;
  }

  /* set the list of periodic booking */
  public set_reservas_periodica(newReservasPeriodica)
  {
    this.reservasPeriodica = newReservasPeriodica;
  }

  /* get the id*/
  public get_id()
  {
   return this.id;
  }

  /* get the nome*/
  public get_nome()
  {
   return this.nome;
  }
 /* get the idufsc */
  public get_idufsc()
  {
    return this.idufsc;
  }

  /* get the email*/
  public get_email()
  {
   return this.email;
  }

  /* get the token*/
  public get_token()
  {
   return this.token;
  }

  /* get the city*/
  public get_cidade()
  {
   return this.cidade;
  }

  /* get the list of single booking */
  public get_reservas_unica()
  {
    return this.reservasUnica ;
  }

  /* get the list of periodic booking */
  public get_reservas_periodica()
  {
    return this.reservasPeriodica;
  }

  public serialize()
  {
    let idcidade=null;let nomecidade=null;
    if(this.cidade != null)
    {
      idcidade = this.cidade.get_id();
      nomecidade = this.cidade.get_nome();
    }
    return {
      id:this.get_id(),
      nome:this.get_nome(),
      email:this.get_email(),
      idufsc:this.get_idufsc(),
      token:this.get_token(),
      cidadeNome:nomecidade,
      cidadeId:idcidade
    };
  }
}
