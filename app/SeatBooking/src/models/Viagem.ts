import {Motorista} from './Motorista';
import {Onibus} from './Onibus';
import {Parada} from './Parada';

export class Viagem
{
  id:number;
  linha:string;
  horario_partida:string;
  horario_chegada:string;
  motorista:Motorista;
  onibus: Onibus;
  paradas = [];

  constructor(id,linha?,horario_partida?,horario_chegada?,motorista?,onibus?,paradas?)
  {
    this.id = id;
    this.linha = linha;
    this.horario_partida = horario_partida;
    this.horario_chegada = horario_chegada;
    this.motorista = motorista;
    this.onibus = onibus;
    this.paradas = paradas;
  }

  public get_id()
  {
    return this.id;
  }

  public get_linha()
  {
    return this.linha;
  }

  public get_horario_partida()
  {
    return this.horario_partida;
  }

  public get_horario_chegada()
  {
    return this.horario_chegada;
  }

  public get_motorista()
  {
    return this.motorista;
  }

  public get_onibus()
  {
    return this.onibus;
  }

  public get_paradas()
  {
    return this.paradas;
  }

  public set_id(newId)
  {
    this.id = newId;
  }

  public set_linha(newLinha)
  {
    this.linha = newLinha;
  }

  public set_horario_partida(newHorario)
  {
    this.horario_partida = newHorario;
  }

  public set_horario_chegada(newHorario)
  {
    this.horario_chegada = newHorario;
  }

  public set_motorista(newMotorista)
  {
    this.motorista = newMotorista;
  }

  public set_onibus(newOnibus)
  {
    this.onibus = newOnibus;
  }

  public set_paradas(newParadas)
  {
    this.paradas = newParadas;
  }
}
