//import { CommonModule } from '@angular/common'; // usar quandofr criar um modulo
import {IonicPageModule} from 'ionic-angular';
import { NgModule } from '@angular/core';
// imports outside the module
import { List_reserves_module } from '../list_reserves/list_reserves.module';
// imports inside the module
import { Login } from './pages/login/login';
import { Logout } from './pages/logout/logout';

@NgModule({
  declarations: [
   Login,
   Logout
  ],
  imports: [
    List_reserves_module,
   IonicPageModule.forChild(Login)
  ],
  //providers: [Service], //se o servce vai ser usado em varios modulos, declarar no app.module, se for usado só nesse modulo ai pod declarar aqui
   entryComponents: [
    Login,
    Logout
   ], // exportar os componentes que serao usados fora do modulo
})
// nao esquece de colocar no import do app.module
export class Authentication_module { }
