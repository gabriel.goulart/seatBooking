import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';

// import pages
import {List_reserves} from '../../../list_reserves/pages/list_reserves/list_reserves';


// import the services
import {Authentication} from '../../services/authentication.service';

/* importing models */
import {Usuario} from "../../../../models/Usuario";

/* importing messges*/
import {MESSAGE_VERIFY_TEXT,
        MESSAGE_AUTHENTICATION_DO_LOGIN,
        MESSAGE_AUTHENTICATION_DO_LOGIN_ERROR_TITLE,
        MESSAGE_AUTHENTICATION_DO_LOGIN_ERRO_TEXT
      } from '../../../../messages';

@Component({
  selector: 'login',
  templateUrl: 'login.html',
  
})
export class Login {
   data:any;
  constructor(public navCtrl: NavController,private loadingCtrl : LoadingController, private alertCtrl : AlertController, private authentication : Authentication) {


  }

  ionViewDidLoad(){
    let loading = this.loadingCtrl.create({content: MESSAGE_VERIFY_TEXT });
    loading.present();
    this.authentication.checkAuthentication().then(res => {
      loading.dismiss();
      this.change_screen();
    }, err => {
      loading.dismiss();
    }).catch(() => {
      loading.dismiss();
    });


  }

  ionViewWillEnter(){

  }

  do_login(){

    let loading = this.loadingCtrl.create({content: MESSAGE_AUTHENTICATION_DO_LOGIN});

    loading.present();

    this.authentication.do_login().subscribe(
      data =>
      {

         this.data += "SUCCESS : " + data + " - info user: nome: " + Usuario.getInstance().get_nome() + " idUfsc : " + Usuario.getInstance().get_idufsc() + " email : " + Usuario.getInstance().get_email() + " - token : " + Usuario.getInstance().get_token();

         loading.dismiss();
         if(data)
         {
           this.change_screen();
         }else{
           this.show_message(MESSAGE_AUTHENTICATION_DO_LOGIN_ERROR_TITLE, MESSAGE_AUTHENTICATION_DO_LOGIN_ERRO_TEXT);
         }
      }, (err) => {
         loading.dismiss();
         this.show_message(MESSAGE_AUTHENTICATION_DO_LOGIN_ERROR_TITLE,MESSAGE_AUTHENTICATION_DO_LOGIN_ERRO_TEXT);
      });

  }

 change_screen()
 {

    this.navCtrl.setRoot(List_reserves);

 }

show_message(title,message)
{
    this.alertCtrl.create({
      title : title,
      subTitle : message,
      buttons : ['OK']
    }).present();
}


}
