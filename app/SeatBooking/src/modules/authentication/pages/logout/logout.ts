import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
//import the components
import {Login} from '../login/login';
// import the services
import {Authentication} from '../../services/authentication.service';
@Component({
  selector: 'logout',
  templateUrl: 'logout.html'
})

export class Logout {

  constructor(public navCtrl: NavController, public authentication : Authentication) {
    this.do_logout();
  }

  do_logout(){
    // calling the service to peform the logout
    if(this.authentication.do_logout())
    {
      this.navCtrl.setRoot(Login);
    }
  }

}
