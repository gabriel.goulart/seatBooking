import {Injectable, EventEmitter} from '@angular/core';
import {Http_Service} from '../../../services/http.service';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import {UserPersistence_Service} from '../../../services/persistence/userPersistence.service';

/* importing the models */
import {Usuario} from "../../../models/Usuario";
import {Cidade} from "../../../models/Cidade";

/* importing the messages*/
import {MESSAGE_HTTP_ERROR} from '../../../messages';
@Injectable()
export class Authentication{

  /**
  * Atributes
  */
  urlAuthentication = new EventEmitter();

  constructor(public http_service:Http_Service,private iab : InAppBrowser, private userPersistence_service:UserPersistence_Service){  }

 /*
 * checking the user's information in internal database
 */
 checkAuthentication()
 {
   return new Promise( (resolve,reject) => {
     this.userPersistence_service.get().then(
       res => {resolve(true);}
       , err => {reject(false);}
     ).catch(
       error => {reject(false);}
     )
   });

 }
/**
* Start login process, first login step (get the url for authenticate the user with idUfsc)
*/
  do_login():EventEmitter<any>{

    /*  this.http_service.exec("http://localhost:8100/api/user/authenticate").then(data => {
        return data;
      }); */

      let request = this.http_service.exec_get("/user/authenticate").subscribe(
        data =>
        {
          request.unsubscribe();
          this.process_login_url_data(data);
          //return dataS;
        }, error =>{
          request.unsubscribe();
          this.urlAuthentication.emit(false);
        });
        return this.urlAuthentication;

  }
  /**
  * Process the data url returned from api
  */
  process_login_url_data(data)
  {
      if(data.success)
      {
        this.openUrlInABrowser(data.url).then((success) => {
          this.register_user(success);
        }).catch((error)=>this.urlAuthentication.emit(false));

      }else{
        this.urlAuthentication.emit(false);
      }

  }

  /**
  * Open a url in inAppbrowser plugin for authenticate with idUFSC platform, second login step
  */
  openUrlInABrowser(url):Promise<any>
  {
       return new Promise((resolve,reject) => {
         let browser = this.iab.create(url,'_blank',{location:'no',clearcache:'yes',hardwareback:'no'});

         let listener = browser.on('loadstop').subscribe((event: any) => {

           if(event.url.indexOf("projetopedagogico.inf.ufsc.br/ine542420171") > -1){
             browser.executeScript({code:"(function(){var x = document.getElementsByTagName('body'); return x[0].innerHTML;})()"}).then(
               (value)=>{
                 listener.unsubscribe();
                 browser.close();
                 resolve(value);
               }
             ).catch(() =>{
               listener.unsubscribe();
               browser.close();
               reject("erro");
             });

           }

         });
       });
  }

/* Register the user in the seatbooking data base*/
 register_user(success)
 {
   let data_to_string = success[0];
   let data_splited = data_to_string.split("<br>");

   Usuario.getInstance().set_idufsc(data_splited[0]);
   Usuario.getInstance().set_nome(data_splited[1]);
   Usuario.getInstance().set_email(data_splited[2]);

   let data = {
     nome: Usuario.getInstance().get_nome(),
     idufsc: Usuario.getInstance().get_idufsc(),
     email: Usuario.getInstance().get_email()
   };

   let request = this.http_service.exec_post("/user/register",data).subscribe(
     data =>
     {
       if(data.success)
       {
         Usuario.getInstance().set_token(data.token);
         Usuario.getInstance().set_id(data.userid);

         if(data.cityname != null && data.cityid != null)
         {
           let cidade = new Cidade(data.cityid,data.cityname);
           Usuario.getInstance().set_cidade(cidade);

         }
         request.unsubscribe();
         this.save_user();
       }else{
         request.unsubscribe();
         this.urlAuthentication.emit(false);
       }



     }, erro => {
       request.unsubscribe();
       this.urlAuthentication.emit(false);
     });


 }

 save_user()
 {
   //this.storage_service.set("usuario_id",Usuario.getInstance().get_id());
   //this.storage_service.set("usuario_nome",Usuario.getInstance().get_nome());
   //this.storage_service.set("usuario_idufsc",Usuario.getInstance().get_idufsc());
   //this.storage_service.set("usuario_email",Usuario.getInstance().get_email());
   //this.storage_service.set("usuario_token",Usuario.getInstance().get_token());
   //let val = {nome: "user1",idade:12};
   this.userPersistence_service.save();
   this.urlAuthentication.emit(true);
 }
  do_logout(): boolean{
    //this.storage_service.set("usuario_id",null);
    //this.storage_service.set("usuario_nome",null);
    //this.storage_service.set("usuario_idufsc",null);
    //this.storage_service.set("usuario_email",null);
    //this.storage_service.set("usuario_token",null);
    Usuario.getInstance().delete_user();
    this.userPersistence_service.clean();
    return true;
  }
}
