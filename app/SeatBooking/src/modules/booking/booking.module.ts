//import { CommonModule } from '@angular/common'; // usar quandofr criar um modulo
import {IonicPageModule} from 'ionic-angular';
import { NgModule } from '@angular/core';



import { Booking } from './pages/booking/booking';
import {City} from './pages/city/city';
import {Travel_Listing_Lines} from './pages/travel/travel_listing_lines/travel_listing_lines';
import {Travel_Info} from './pages/travel/travel_info/travel_info';
import {Travel_Date_Time} from './pages/travel/travel_date_time/travel_date_time';
import {Travel_Single_Booking} from './pages/travel/travel_booking/travel_single_booking/travel_single_booking';
import {Travel_Periodic_Booking} from './pages/travel/travel_booking/travel_periodic_booking/travel_periodic_booking';

@NgModule({
  declarations: [
   Booking,
   City,
   Travel_Listing_Lines,
   Travel_Info,
   Travel_Date_Time,
   Travel_Single_Booking,
   Travel_Periodic_Booking
  ],
  imports: [
   IonicPageModule.forChild(Booking)
  ],
   entryComponents: [
    Booking,
    City,
    Travel_Listing_Lines,
    Travel_Info,
    Travel_Date_Time,
    Travel_Single_Booking,
    Travel_Periodic_Booking
   ], // exportar os componentes que serao usados fora do modulo
})
// nao esquece de colocar no import do app.module
export class Booking_module { }
