import { Component } from '@angular/core';
import { NavController,LoadingController, Platform } from 'ionic-angular';
import {Booking_Service} from '../../services/booking.service';

// importing pages
import {City} from '../city/city';
import {Travel_Listing_Lines} from '../travel/travel_listing_lines/travel_listing_lines';

// importing models
import {Usuario} from '../../../../models/Usuario';

// importing the message text


@Component({
  selector: 'booking',
  templateUrl: 'booking.html'
})
export class Booking {

  constructor(public navCtrl: NavController,private platform: Platform, private booking_service : Booking_Service,private loadingCtrl : LoadingController) {

  }

  ionViewDidLoad(){

    this.platform.ready().then(() => {
      if(Usuario.getInstance().get_cidade() != null)
      {
        this.navCtrl.setRoot(Travel_Listing_Lines);
      }else{
        this.navCtrl.setRoot(City);
      }
    });
  }


}
