import { Component } from '@angular/core';
import { NavController,LoadingController, Platform,AlertController} from 'ionic-angular';


// importing services
import {Booking_Service} from '../../services/booking.service';
import {UserPersistence_Service} from '../../../../services/persistence/userPersistence.service';
// importing models
import {Cidade} from '../../../../models/Cidade';
import {Usuario} from '../../../../models/Usuario';

//importing pages
import {Travel_Listing_Lines} from '../travel/travel_listing_lines/travel_listing_lines';
import {List_reserves} from '../../../list_reserves/pages/list_reserves/list_reserves';

// importing the message text
import {
        MESSAGE_BOOKING_SEARCHING_LOCATION,
        MESSAGE_BOOKING_SEARCHING_LOCATION_ERROR,
        MESSAGE_YES,
        MESSAGE_NO,
        MESSAGE_BOOKING_SEARCHING_LOCATION_USE_CURRENT_CITY,
        MESSAGE_BOOKING_NO_CITY,
        MESSAGE_REFRESH_PULL_TEXT,
        MESSAGE_REFRESH_REFRESHING_TEXT,
        MESSAGE_BOOKING_UPDATING_CITY,
        MESSAGE_ERROR,
        MESSAGE_HTTP_ERROR
} from '../../../../messages';

@Component({
  selector: 'city',
  templateUrl: 'city.html'
})
export class City {
  nomeCidade:any;
  idCidade:any;
  cities:any;
  showCities:boolean = false;
  noCities:boolean = false;
  message_no_cities = MESSAGE_BOOKING_NO_CITY;
  massage_refresh_pull_text = MESSAGE_REFRESH_PULL_TEXT;
  massage_refresh_refreshing_text = MESSAGE_REFRESH_REFRESHING_TEXT;
  searchTerm: string = '';
  cities_filtered:any;
  refresher:any;
  //loadingMessage:any;
  constructor(public navCtrl: NavController,private platform: Platform, private booking_service : Booking_Service,private loadingCtrl : LoadingController,private alertCtrl : AlertController,private userPersistence_service:UserPersistence_Service) {

  }

  ionViewDidLoad(){
    //this.latitude += "passou aqui";
    this.platform.ready().then(() => {
      let loadingMessage = this.loadingCtrl.create({content: MESSAGE_BOOKING_SEARCHING_LOCATION });
      loadingMessage.present();
  //    let options = { enableHighAccuracy : true };
  //    this.geolacation.getCurrentPosition(options).then( (resp ) => {
  //      loadingMessage.dismiss();
  //      this.latitude = "valor " +  resp.coords.latitude;
  //      this.longitude = "valor " + resp.coords.longitude;
  //    }).catch((error) => {
  //      loadingMessage.dismiss();
  //      this.latitude = "ERROR " + error;
  //    });

      this.booking_service.getLocation().then( (resp) => {
        loadingMessage.dismiss();
        this.get_city(resp);
      }).catch((error) => {
        loadingMessage.dismiss();
        this.show_message(MESSAGE_BOOKING_SEARCHING_LOCATION_ERROR,true);

      });
    });
  }

 /* get the city which was returned from the service*/
  get_city(data)
  {
    //this.latitude = "valor : " +  data.latitude;
    //this.longitude = "valor  : " + data.longitude;
    if(data != null)
    {

      this.nomeCidade = data[0].nome;
      this.idCidade = data[0].idcidade;
      this.show_message(this.nomeCidade,false);

    }else{
      this.show_message(MESSAGE_BOOKING_SEARCHING_LOCATION_ERROR,true);
    }

  }


 /* get a list of citites available*/
  get_cities()
  {
    this.booking_service.getCities().then(data =>{
      if(data != null)
      {
        if(data != "")
        {
          this.cities = data;
          this.cities_filtered = this.cities;
          this.showCities = true;
          this.noCities = false;
          this.refresher.complete();
        }else{
         this.noCities = true;
         this.showCities = false;
         this.refresher.complete();
       }

      }else{
        this.refresher.complete();
        this.show_message(MESSAGE_HTTP_ERROR,true,true);
        //this.go_back_screen();
      }
    }).catch(() => {
      this.refresher.complete();
      this.show_message(MESSAGE_HTTP_ERROR,true,true);
    });

  }

 /* set the user's city*/
  set_city()
  {
    let cidade = new Cidade(this.idCidade,this.nomeCidade);
    Usuario.getInstance().set_cidade(cidade);

    let userCity = {
      userid: Usuario.getInstance().get_id(),
      cityid: Usuario.getInstance().get_cidade().get_id()
    };
    let loadingMessage = this.loadingCtrl.create({content: MESSAGE_BOOKING_UPDATING_CITY });
    loadingMessage.present();
    this.booking_service.setCity(userCity).then(
        (data) => {
          this.userPersistence_service.save();
          loadingMessage.dismiss();
          this.change_screen();
        }
    ).catch(error => {
      loadingMessage.dismiss();
      this.show_message(MESSAGE_HTTP_ERROR,true,true);
    });


  }


 /*get the city which was selected in view*/
  city_selected(city)
  {
    this.idCidade = city.idcidade;
    this.nomeCidade = city.nome;

    this.set_city();
    //this.show_message(city.idcidade,true);
  }

 /* refresh the page*/
  doRefresh(refresher){
    this.refresher = refresher;
    this.get_cities();
  }

 /* searchBar configuration */
 filterItems(searchTerm){

        return this.cities.filter((item) => {
            return item.nome.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });

  }

  setFilteredItems() {

        this.cities_filtered = this.filterItems(this.searchTerm);

    }
  /* it shows all messages and alerts to user*/
  show_message(message,error:boolean,alert:boolean = false)
  {
    if(!error)
    {
      this.alertCtrl.create({
        title : MESSAGE_BOOKING_SEARCHING_LOCATION_USE_CURRENT_CITY,
        subTitle : message,
        buttons : [
          {
            text: MESSAGE_YES,
            handler: data =>{
              this.set_city();
            }
          },
          {
            text: MESSAGE_NO,
            handler: data =>{
              this.get_cities();
            }
          }
      ]
      }).present();
    }else if(!alert){
      this.alertCtrl.create({
        title : MESSAGE_ERROR,
        subTitle : message,
        buttons : [
          {
            text:'OK',
            handler: data =>{
              this.get_cities();
              //this.go_back_screen();
            }
      }]
      }).present();
    }else{
      this.alertCtrl.create({
        title : MESSAGE_ERROR,
        subTitle : message,
        buttons : [
          {
            text:'OK',
            handler: data =>{
              //this.get_cities();
              this.go_back_screen();
            }
      }]
      }).present();
    }
  }

  change_screen()
  {
    this.navCtrl.setRoot(Travel_Listing_Lines);
  }

  go_back_screen()
  {
    this.navCtrl.setRoot(List_reserves);
  }

}
