import { Component } from '@angular/core';
import { NavController,LoadingController,Platform,NavParams,ModalController,AlertController,ToastController } from 'ionic-angular';

// importing services
import {Booking_Service} from '../../../../services/booking.service';

// importing models
import {Usuario} from '../../../../../../models/Usuario';

// importing pages
import {List_reserves} from '../../../../../list_reserves/pages/list_reserves/list_reserves';
import {Travel_Single_Booking} from '../travel_single_booking/travel_single_booking';

// importing the message text
import {
        TITLE_PAGE_BOOKING_PERIODIC_BOOKING,
        MESSAGE_BOOKING_INFO_ERROR,
        MESSAGE_ERROR,
        MESSAGE_BOOKING_SINGLE_PERIODIC_SEAT_SELECTED,
        MESSAGE_CANCEL,
        MESSAGE_BOOKING_SINGLE_PERIODIC_DO_BOOKING,
        MESSAGE_BOOKING_SINGLE_PERIODIC_DO_BOOKING_SUCCESS,
        MESSAGE_HTTP_ERROR,
        TEXT_BOOKING_SINGLE_PERIODIC_DRIVER_SEAT,
        TEXT_BOOKING_SINGLE_PERIODIC_FREE_SEAT,
        TEXT_BOOKING_SINGLE_PERIODIC_BUSY_SEAT,
        TEXT_BOOKING_SINGLE_PERIODIC_PREFERENTIAL_SEAT,
        TEXT_BOOKING_SINGLE_PERIODIC_SEAT_ALREADY_BOOKED,
        TEXT_BOOKING_SINGLE_PERIODIC_CHANGE_SEAT,
        MESSAGE_BOOKING_GETTING_TRAVEL_INFO
} from '../../../../../../messages';

@Component({
  selector: 'travel_periodic_booking',
  templateUrl: 'travel_periodic_booking.html'
})
export class Travel_Periodic_Booking {
  title:any = TITLE_PAGE_BOOKING_PERIODIC_BOOKING;
  driverSeat:any = TEXT_BOOKING_SINGLE_PERIODIC_DRIVER_SEAT;
  freeSeat:any = TEXT_BOOKING_SINGLE_PERIODIC_FREE_SEAT;
  busySeat:any = TEXT_BOOKING_SINGLE_PERIODIC_BUSY_SEAT;
  preferentialSeat:any= TEXT_BOOKING_SINGLE_PERIODIC_PREFERENTIAL_SEAT;
  erroTitle:any = TEXT_BOOKING_SINGLE_PERIODIC_SEAT_ALREADY_BOOKED;
  textbuttonChangeSeat:any = TEXT_BOOKING_SINGLE_PERIODIC_CHANGE_SEAT;
  /* ATRIBUTES */
  date:any;
  travel:any;
  seats:any;
  seatSelected:any;
  dates:any=[];
  showError=false;
  error:any;
  weekDays:any=[];


  constructor(public navCtrl: NavController,
              private platform: Platform,
              private loadingCtrl : LoadingController,
              private navParams: NavParams,
              private alertCtrl : AlertController,
              private modalCtrl: ModalController,
              private toastCtrl: ToastController,
              private booking_service : Booking_Service
              ) {

  }

  ionViewDidLoad(){
    //this.latitude += "passou aqui";
    this.platform.ready().then(() => {

      if(this.navParams.get('success'))
      {

        this.travel = this.navParams.get('travel');
        this.dates[0] = this.navParams.get('initialdate');
        this.dates[1] = this.navParams.get('finalldate');
        this.weekDays = this.navParams.get('weekDays');
        this.seats = this.travel.get_onibus().get_assentos();

      }else{
          this.show_message(MESSAGE_ERROR,MESSAGE_BOOKING_INFO_ERROR,true);
      }

    });
  }

  /*
  * gets the seat selected on user interface
  *@seat is a seat which was selected by user
  */
   seat_selected(seat)
   {

       this.seatSelected = seat;
       let dado = seat.get_nome();
       this.show_message(MESSAGE_BOOKING_SINGLE_PERIODIC_SEAT_SELECTED,dado,false);

   }

   /*
   * does the seat booking
   */


    register_booking()
    {
      //this.testearray = this.weekDays[0]+"-"+this.weekDays[1]+"-"+this.weekDays[2]+"-"+this.weekDays[3]+"-"+this.weekDays[4]+"-"+this.weekDays[5];

      let data = {
        datainicial : this.dates[0],
        datafinal : this.dates[1],
        diasemana: this.weekDays.toString(),
        idassento : this.seatSelected.get_id(),
        idusuario : Usuario.getInstance().get_id(),
        idviagem : this.travel.get_id()
      };

      let loadingMessage = this.loadingCtrl.create({content: MESSAGE_BOOKING_SINGLE_PERIODIC_DO_BOOKING });
      loadingMessage.present();
      this.booking_service.createPeriodicBooking(data).then(
        data => {
             loadingMessage.dismiss();
             if(data != null && data != "")
             {
               this.toastCtrl.create(
                {
                  message : MESSAGE_BOOKING_SINGLE_PERIODIC_DO_BOOKING_SUCCESS,
                  duration: 5000,
                  position: "top"
                }
               ).present();

               this.change_screen();
             }else{
               this.show_message(MESSAGE_ERROR,MESSAGE_HTTP_ERROR,true);
             }
        }
      ).catch(
        err => {
          loadingMessage.dismiss();
          if(err.message != null && err.message != "")
          {
            this.error = err.message;

            this.showError = true;
            //this.show_message(MESSAGE_ERROR,err.message,true);
          }else{
             this.show_message(MESSAGE_ERROR,MESSAGE_HTTP_ERROR,true);
          }
        }
      );
    }

  /*
  * changes seat which came from a error
  *@i is the error index in the array
  *@erro is the object error
  */
  change_seat(i,error)
  {

    let loadingMessage = this.loadingCtrl.create({content: MESSAGE_BOOKING_GETTING_TRAVEL_INFO });
    loadingMessage.present();
    this.booking_service.getTravel(this.travel,error.date).then(
      data => {
          if(data != null)
          {
            loadingMessage.dismiss();
            this.open_modal(data,error.date,i);
          }else{

            loadingMessage.dismiss();
            this.show_message(MESSAGE_ERROR,MESSAGE_HTTP_ERROR,true,false);
        }
      }
    ).catch(
      err => {

        loadingMessage.dismiss();
        this.show_message(MESSAGE_ERROR,MESSAGE_HTTP_ERROR,true,false);
      }
    );
  }

  open_modal(travelSelected,date,index)
  {
    let modal = this.modalCtrl.create(Travel_Single_Booking,{success:true,date:date,travel:travelSelected,changeSeat:true});

    modal.onDidDismiss(data =>{
      if(data.success)
      {
        this.error.splice(index,1);

        if(this.error.length <= 0)
        {
          this.toastCtrl.create(
           {
             message : MESSAGE_BOOKING_SINGLE_PERIODIC_DO_BOOKING_SUCCESS,
             duration: 5000,
             position: "top"
           }
          ).present();

          this.change_screen();
        }
      }else if(data.alreadyHaveBooking){
        this.error.splice(index,1);
      }

    });
    modal.present();
  }
  /*
  * shows all the error MESSAGES
  * @message is a message text to user
  */
    show_message(title,message, error:boolean,change=true)
    {
      if(!error)
      {
        this.alertCtrl.create({
          title : title,
          subTitle : message,
          buttons : [
            {
              text:'OK',
              handler: data =>{
                this.register_booking();
              }
            },
              {
                text: MESSAGE_CANCEL
              }
        ]
        }).present();
      }else{
        this.alertCtrl.create({
          title : title,
          subTitle : message,
          buttons : [
            {
              text:'OK',
              handler: data =>{
                //this.get_cities();
                if(change){this.go_back_screen();}

              }
        }]
        }).present();
      }

    }

    /*
    * it goes to another screen
    */
    change_screen()
    {
      this.navCtrl.setRoot(List_reserves);
    }
   /*
   * it goes back to the last screen
   */
    go_back_screen()
    {
      this.navCtrl.pop();
    }

}
