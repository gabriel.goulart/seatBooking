import { Component } from '@angular/core';
import { NavController,NavParams,LoadingController,AlertController,ViewController, Platform ,ToastController } from 'ionic-angular';

// importing services
import {Booking_Service} from '../../../../services/booking.service';

// importing models
import {Usuario} from '../../../../../../models/Usuario';
import {Viagem} from '../../../../../../models/Viagem';

// importing the message text
import {
        TITLE_PAGE_BOOKING_SINGLE_BOOKING,
        MESSAGE_REFRESH_PULL_TEXT,
        MESSAGE_REFRESH_REFRESHING_TEXT,
        MESSAGE_BOOKING_SEARCHING_LINES,
        MESSAGE_BOOKING_INFO_ERROR,
        MESSAGE_ERROR,
        MESSAGE_BOOKING_SINGLE_PERIODIC_SEAT_SELECTED,
        MESSAGE_CANCEL,
        MESSAGE_BOOKING_SINGLE_PERIODIC_DO_BOOKING,
        MESSAGE_BOOKING_SINGLE_PERIODIC_DO_BOOKING_SUCCESS,
        MESSAGE_BOOKING_SINGLE_PERIODIC_EDITING_BOOKING,
        MESSAGE_HTTP_ERROR,
        TEXT_BOOKING_SINGLE_PERIODIC_DRIVER_SEAT,
        TEXT_BOOKING_SINGLE_PERIODIC_FREE_SEAT,
        TEXT_BOOKING_SINGLE_PERIODIC_BUSY_SEAT,
        TEXT_BOOKING_SINGLE_PERIODIC_PREFERENTIAL_SEAT

} from '../../../../../../messages';

// importing pages
import {Travel_Date_Time} from '../../travel_date_time/travel_date_time';
import {List_reserves} from '../../../../../list_reserves/pages/list_reserves/list_reserves';

@Component({
  selector: 'travel_single_booking',
  templateUrl: 'travel_single_booking.html'
})
export class Travel_Single_Booking {
  title:any = TITLE_PAGE_BOOKING_SINGLE_BOOKING;
  driverSeat:any = TEXT_BOOKING_SINGLE_PERIODIC_DRIVER_SEAT;
  freeSeat:any = TEXT_BOOKING_SINGLE_PERIODIC_FREE_SEAT;
  busySeat:any = TEXT_BOOKING_SINGLE_PERIODIC_BUSY_SEAT;
  preferentialSeat:any= TEXT_BOOKING_SINGLE_PERIODIC_PREFERENTIAL_SEAT;
  /* ATRIBUTES */
  date:any;
  travel:any;
  seats:any;
  seatSelected:any;
  showSeats=false;
  changeSeat=false;
  updateSeat=false;
  alreadyHaveBooking=false;

  constructor(public navCtrl: NavController,
              private navParams: NavParams,
              private platform: Platform,
              private loadingCtrl : LoadingController,
              private alertCtrl : AlertController,
              private toastCtrl: ToastController,
              private viewCtrl: ViewController,
              private booking_service : Booking_Service
            ) { }

/*
* it calls the select date/time screen
*/
  ionViewDidLoad(){

    this.platform.ready().then(() => {

      if(this.navParams.get('success'))
      {
        this.date = this.navParams.get('date');
        this.travel = this.navParams.get('travel');
        this.seats = this.travel.get_onibus().get_assentos();
        if(this.navParams.get('changeSeat'))
        {
          this.changeSeat = this.navParams.get('changeSeat');
        }
        if(this.navParams.get('updateSeat'))
        {
          this.updateSeat = this.navParams.get('updateSeat');
        }
        this.showSeats = true;
      }else{
        this.show_message(MESSAGE_ERROR,MESSAGE_BOOKING_INFO_ERROR,true);
      }

      //this.line.id = this.navParams.get('lineid');
      //this.line.nome = this.navParams.get('linename');

  /*  let modal = this.modalCtrl.create(Travel_Date_Time,{lineid: this.line.id});


      modal.onDidDismiss(data =>{
        if(data.success)
        {
          this.date = data.date;
          this.travel = data.travel;
          this.seats = this.travel.get_onibus().get_assentos();
          this.showSeats = true;
        }else{
            this.show_message(MESSAGE_ERROR,MESSAGE_BOOKING_INFO_ERROR,true);
        }

      });
      modal.present(); */


    });
  }

/*
* gets the seat selected on user interface
*@seat is a seat which was selected by user
*/
 seat_selected(seat)
 {
   if(!seat.get_ocupado())
   {
     this.seatSelected = seat;
     let dado = seat.get_nome();
     this.show_message(MESSAGE_BOOKING_SINGLE_PERIODIC_SEAT_SELECTED,dado,false);
   }

 }

/*
* does the seat booking
*/
 register_booking()
 {
   let data = {
     data : this.date,
     idassento : this.seatSelected.get_id(),
     idusuario : Usuario.getInstance().get_id(),
     idviagem : this.travel.get_id(),
   };
   let loadingMessage = this.loadingCtrl.create({content: MESSAGE_BOOKING_SINGLE_PERIODIC_DO_BOOKING });
   loadingMessage.present();
   if(!this.updateSeat)
   {
     this.booking_service.createBooking(data).then(
       data => {
            loadingMessage.dismiss();
            if(data != null && data != "")
            {
              this.toastCtrl.create(
               {
                 message : MESSAGE_BOOKING_SINGLE_PERIODIC_DO_BOOKING_SUCCESS,
                 duration: 5000,
                 position: "top"
               }
              ).present();

              this.change_screen(true);
            }else{
              this.show_message(MESSAGE_ERROR,MESSAGE_HTTP_ERROR,true);
            }
       }
     ).catch(
       err => {
         loadingMessage.dismiss();
         if(err.message != null && err.message != "")
         {
           this.alreadyHaveBooking = true;
           this.show_message(MESSAGE_ERROR,err.message,true);
         }else{
            this.show_message(MESSAGE_ERROR,MESSAGE_HTTP_ERROR,true);
         }
       }
     );
   }else{
     this.booking_service.updateBooking(data).then(
       data => {
            loadingMessage.dismiss();
            if(data != null && data != "")
            {
              this.toastCtrl.create(
               {
                 message : MESSAGE_BOOKING_SINGLE_PERIODIC_EDITING_BOOKING,
                 duration: 5000,
                 position: "top"
               }
              ).present();

              this.change_screen(true);
            }else{
              this.show_message(MESSAGE_ERROR,MESSAGE_HTTP_ERROR,true);
            }
       }
     ).catch(
       err => {
         loadingMessage.dismiss();
         if(err.message != null && err.message != "")
         {
           this.show_message(MESSAGE_ERROR,err.message,true);
         }else{
            this.show_message(MESSAGE_ERROR,MESSAGE_HTTP_ERROR,true);
         }
       }
     );

   }

 }
/*
* shows all the error MESSAGES
* @message is a message text to user
*/
  show_message(title,message, error:boolean)
  {
    if(!error)
    {
      this.alertCtrl.create({
        title : title,
        subTitle : message,
        buttons : [
          {
            text:'OK',
            handler: data =>{
              this.register_booking();
            }
          },
            {
              text: MESSAGE_CANCEL
            }
      ]
      }).present();
    }else{
      this.alertCtrl.create({
        title : title,
        subTitle : message,
        buttons : [
          {
            text:'OK',
            handler: data =>{
              this.change_screen(false);
            }
      }]
      }).present();
    }

  }

  /*
  * it goes to another screen
  */
  change_screen(success)
  {
    if(this.changeSeat || this.updateSeat)
    {
      this.viewCtrl.dismiss({success:success,alreadyHaveBooking:this.alreadyHaveBooking});
    }else{
      this.navCtrl.setRoot(List_reserves);
    }

  }
 /*
 * it goes back to the last screen
 */
  go_back_screen(success)
  {
    if(this.changeSeat || this.updateSeat)
    {
      this.viewCtrl.dismiss({success:success,alreadyHaveBooking:this.alreadyHaveBooking});
    }else{
      this.navCtrl.pop();
    }

  }




}
