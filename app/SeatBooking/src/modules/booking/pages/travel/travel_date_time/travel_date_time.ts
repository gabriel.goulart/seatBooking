import { Component } from '@angular/core';
import { NavController,ViewController,NavParams,LoadingController, Platform ,AlertController,ActionSheetController} from 'ionic-angular';
import { DatePicker } from '@ionic-native/date-picker';

// importing services
import {Booking_Service} from '../../../services/booking.service';

// importing models
import {Usuario} from '../../../../../models/Usuario';

//importing pages
import {List_reserves} from '../../../../list_reserves/pages/list_reserves/list_reserves';
import {City} from '../../city/city';
import {Travel_Periodic_Booking} from '../travel_booking/travel_periodic_booking/travel_periodic_booking';

// importing the message text
import {
        TITLE_PAGE_BOOKING_SELECT_DATE_TIME,
        TEXT_BOOKING_SELECT_DATE,
        TEXT_BOOKING_SELECT_TIME,
        TEXT_BOOKING_BUTTON_CHANGE_DATE,
        TEXT_BOOKING_SELECT_INITIAL_DATE,
        TEXT_BOOKING_SELECT_FINAL_DATE,
        TEXT_BOOKING_SELECT_WEEK_DAYS,
        TEXT_BOOKING_WEEK_DAY_ALL,
        TEXT_BOOKING_WEEK_DAY_MONDAY,
        TEXT_BOOKING_WEEK_DAY_TUESDAY,
        TEXT_BOOKING_WEEK_DAY_WEDNESDAY,
        TEXT_BOOKING_WEEK_DAY_THURSDAY,
        TEXT_BOOKING_WEEK_DAY_FRIDAY,
        MESSAGE_CANCEL,
        MESSAGE_BOOKING_GETTING_TRAVELS,
        MESSAGE_BOOKING_GETTING_TRAVEL_INFO,
        MESSAGE_BOOKING_SELECT_DATE_AGAIN,
        MESSAGE_ERROR,
        MESSAGE_HTTP_ERROR
} from '../../../../../messages';

@Component({
  selector: 'travel_date_time',
  templateUrl: 'travel_date_time.html'
})
export class Travel_Date_Time {

  title:any = TITLE_PAGE_BOOKING_SELECT_DATE_TIME;
  textSelectDate =TEXT_BOOKING_SELECT_DATE;
  textSelectTime = TEXT_BOOKING_SELECT_TIME;
  textSelectInitialDate = TEXT_BOOKING_SELECT_INITIAL_DATE;
  textSelectFinalDate = TEXT_BOOKING_SELECT_FINAL_DATE;
  textSelectWeekDay = TEXT_BOOKING_SELECT_WEEK_DAYS;
  buttonCancel = MESSAGE_CANCEL;

  /* ATRIBUTES */
  lineid:any;
  showTimePicker:boolean = false;
  dateSelected:any = "";
  datesSelected:any=[];
  dateWeekDay:any;
  actualDate:any;
  travels:any;
  nextPage=null;
  isPeriodicBooking:boolean= false;
  showWeekDays:boolean=false;
  isChecked:boolean=false;
  weekdaysSelected:any = [false,false,false,false,false,false];
  weekDays:any = [
    {
      id:0,
      name:TEXT_BOOKING_WEEK_DAY_ALL
    },
    {
      id:1,
      name:TEXT_BOOKING_WEEK_DAY_MONDAY
    },
    {
      id:2,
      name:TEXT_BOOKING_WEEK_DAY_TUESDAY
    },
    {
      id:3,
      name:TEXT_BOOKING_WEEK_DAY_WEDNESDAY
    },
    {
      id:4,
      name:TEXT_BOOKING_WEEK_DAY_THURSDAY
    },
    {
      id:5,
      name:TEXT_BOOKING_WEEK_DAY_FRIDAY
    }
  ];
  constructor(public navCtrl: NavController,
              private platform: Platform,
              private loadingCtrl : LoadingController,
              private alertCtrl : AlertController,
              private actionSheetCtrl: ActionSheetController,
              private navParams: NavParams,
              private datePicker: DatePicker,
              private booking_service : Booking_Service,
              private viewCtrl: ViewController
            ) {

  }

  ionViewDidLoad(){

    this.platform.ready().then(() => {

      this.lineid = this.navParams.get('lineid');
      this.nextPage = this.navParams.get('nextpage');
      if(this.navParams.get('isPeriodicBooking'))
      {
        this.isPeriodicBooking = true;
      }
      this.select_date();



    });
  }

  select_date(indexTime=-1)
  {
    //this.showDatePicker = true;

    let dateMin = new Date().toString();

    this.datePicker.show({
      titleText :TEXT_BOOKING_SELECT_DATE ,
      date: new Date(),
      mode: 'date',
      minDate:dateMin,
      allowOldDates : false
    }).then(
      date => {
        this.dateSelected = "";
        let day = date.getUTCDate();
        if(day < 10)
        {
          this.dateSelected = "0";
        }
        this.dateSelected += day + "-";

        let month = 1+date.getUTCMonth();
        if(month < 10)
        {
          this.dateSelected += "0";
        }
        this.dateSelected += month;
        this.dateSelected += "-" + date.getUTCFullYear();
        this.dateWeekDay = date.getUTCDay();

        if(this.isPeriodicBooking)
        {
           if(indexTime == -1)
           {
             this.datesSelected.push(this.dateSelected);
           }else{
             this.datesSelected[indexTime] = this.dateSelected;
           }

           if(this.datesSelected.length > 1)
           {
             this.showWeekDays = true;
             this.get_time();
           }

        }else{
          this.get_time();
        }


      },
      err => {

        if(this.dateSelected == '')
        {
          let data = {
            success:false,
          };
          this.change_screen(data);
        }

      }
    );
  }

  get_time()
  {
    let loadingMessage = this.loadingCtrl.create({content: MESSAGE_BOOKING_GETTING_TRAVELS });
    loadingMessage.present();
    this.booking_service.getTravels(this.lineid).then(
      data => {
        this.travels = data;
        loadingMessage.dismiss();
        this.showTimePicker = true;
      },
      err => {
        let data = {
          success:false,
        };
        loadingMessage.dismiss();
        this.change_screen(data);
      }
    ).catch(
      () => {
        let data = {
          success:false,
        };
        this.change_screen(data);
      }
    );

  }

  travel_selected(travelSelected)
  {
    if(this.dateSelected != "" || this.dateSelected != null)
    {

      this.get_travel_information(travelSelected);

    }else{
      this.show_message(MESSAGE_ERROR,MESSAGE_BOOKING_SELECT_DATE_AGAIN);
    }
  }

/*
* get all the travel's informations
* @travel travel selected by user
*/
  get_travel_information(travel)
  {
    let loadingMessage = this.loadingCtrl.create({content: MESSAGE_BOOKING_GETTING_TRAVEL_INFO });
    loadingMessage.present();
    this.booking_service.getTravel(travel,this.dateSelected).then(
      data => {
          if(data != null)
          {
            let dado;

            if(this.isPeriodicBooking)
            {
              dado = {
                success:true,
                travel:data,
                initialdate:this.datesSelected[0],
                finalldate:this.datesSelected[1],
                weekDays:this.weekdaysSelected
              };

            }else{
              dado = {
                success:true,
                travel:data,
                date:this.dateSelected
              };
            }

            loadingMessage.dismiss();
            this.change_screen(dado);
          }else{
            let data = {
              success:false,
            };
            loadingMessage.dismiss();
            this.change_screen(data);
        }
      }
    ).catch(
      err => {
        let data = {
          success:false,
        };
        loadingMessage.dismiss();
        this.change_screen(data);
      }
    );
  }

  checkbox_change(event,weekDayId)
  {
    /* if(event.checked)
     {
       this.weekdaysSelected[weekDayId] = "true";
     }else{
       this.weekdaysSelected[weekDayId] = "false";
     } */

     if(event.checked && weekDayId == 0)
     {
       for(let i=0; i < this.weekdaysSelected.length;i++){
         this.weekdaysSelected[i] = true;
       }
     }else if(weekDayId == 0){
       for(let i=0; i < this.weekdaysSelected.length;i++){
         this.weekdaysSelected[i] = false;
       }
     }



  }

  show_message(title,message)
  {
    this.alertCtrl.create({
      title : title,
      subTitle : message,
      buttons : [
        {
          text:'OK',
          handler: data =>{
            this.select_date();
          }
        }
    ]
    }).present();
  }

  change_screen(data){
    //this.viewCtrl.dismiss(data);
    this.navCtrl.push(this.nextPage,data);
  }

  go_back_screen()
  {
    this.navCtrl.pop();
  }


}
