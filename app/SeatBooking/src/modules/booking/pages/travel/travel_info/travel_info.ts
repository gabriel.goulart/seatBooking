import { Component } from '@angular/core';
import { NavController,NavParams,LoadingController, Platform ,AlertController,ActionSheetController,ModalController} from 'ionic-angular';

import {Booking_Service} from '../../../services/booking.service';

// importing models
import {Usuario} from '../../../../../models/Usuario';
import {Viagem} from '../../../../../models/Viagem';

//importing pages
import {List_reserves} from '../../../../list_reserves/pages/list_reserves/list_reserves';
import {City} from '../../city/city';
import {Travel_Date_Time_Modal} from '../travel_modal_date_time/travel_date_time.modal';

// importing the message text
import {
        MESSAGE_BOOKING_NO_LINES,
        MESSAGE_REFRESH_PULL_TEXT,
        MESSAGE_REFRESH_REFRESHING_TEXT,
        MESSAGE_BOOKING_SEARCHING_LINES,
        MESSAGE_BOOKING_LINE_ACTION_INFO,
        MESSAGE_BOOKING_LINE_ACTION_BOOKING,
        MESSAGE_BOOKING_LINE_ACTION_BOOKING_PERIODIC,
        MESSAGE_BOOKING_LINE_ACTION_CANCEL,
        TEXT_BOOKING_GENERAL_TRAVEL_INFO,
        TEXT_BOOKING_GENERAL_TRAVEL_INFO_LINE,
        TEXT_BOOKING_GENERAL_TRAVEL_INFO_DEPARTURE_TIME,
        TEXT_BOOKING_GENERAL_TRAVEL_INFO_ARRIVAL_TIME,
        TEXT_BOOKING_BUS_TRAVEL_INFO,
        TEXT_BOOKING_BUS_TRAVEL_INFO_MODEL,
        TEXT_BOOKING_BUS_TRAVEL_INFO_BRAND,
        TEXT_BOOKING_BUS_TRAVEL_INFO_AMOUNT_SEATS,
        TEXT_BOOKING_BUS_TRAVEL_INFO_AMOUNT_SEATS_PREFERENTIAL,
        TEXT_BOOKING_BUS_TRAVEL_INFO_AMOUNT_SEATS_BOOKED,
        TEXT_BOOKING_DRIVER_TRAVEL_INFO,
        TEXT_BOOKING_DRIVER_TRAVEL_INFO_NAME,
        TEXT_BOOKING_DRIVER_TRAVEL_EMAIL,
        MESSAGE_BOOKING_INFO_ERROR,
        TEXT_BOOKING_STOP_TRAVEL_INFO,
        MESSAGE_ERROR,
        MESSAGE_HTTP_ERROR
} from '../../../../../messages';

@Component({
  selector: 'travel_info',
  templateUrl: 'travel_info.html'
})
export class Travel_Info {
  /* MESSAGES AND TEXTS */
  massage_refresh_pull_text = MESSAGE_REFRESH_PULL_TEXT;
  massage_refresh_refreshing_text = MESSAGE_REFRESH_REFRESHING_TEXT;
  text_general_travel_info = TEXT_BOOKING_GENERAL_TRAVEL_INFO;
  text_general_travel_info_line = TEXT_BOOKING_GENERAL_TRAVEL_INFO_LINE;
  text_general_travel_departure_time = TEXT_BOOKING_GENERAL_TRAVEL_INFO_DEPARTURE_TIME;
  text_general_travel_arrival_time = TEXT_BOOKING_GENERAL_TRAVEL_INFO_ARRIVAL_TIME;
  text_bus_travel_info = TEXT_BOOKING_BUS_TRAVEL_INFO;
  text_bus_travel_info_model = TEXT_BOOKING_BUS_TRAVEL_INFO_MODEL;
  text_bus_travel_info_brand = TEXT_BOOKING_BUS_TRAVEL_INFO_BRAND;
  text_bus_travel_info_amount_seats = TEXT_BOOKING_BUS_TRAVEL_INFO_AMOUNT_SEATS;
  text_bus_travel_info_amount_seats_preferential = TEXT_BOOKING_BUS_TRAVEL_INFO_AMOUNT_SEATS_PREFERENTIAL;
  text_bus_travel_info_amount_seats_booked = TEXT_BOOKING_BUS_TRAVEL_INFO_AMOUNT_SEATS_BOOKED;
  text_driver_travel_info = TEXT_BOOKING_DRIVER_TRAVEL_INFO;
  text_driver_travel_info_name = TEXT_BOOKING_DRIVER_TRAVEL_INFO_NAME;
  text_driver_travel_info_email = TEXT_BOOKING_DRIVER_TRAVEL_EMAIL;
  text_stop_travel_info = TEXT_BOOKING_STOP_TRAVEL_INFO;

  /* ATRIBUTES */
  line:any= {id:null,nome:null};
  date:any;
  travel:any;
  showInfo=false;



  constructor(public navCtrl: NavController,
              private platform: Platform,
              private loadingCtrl : LoadingController,
              private alertCtrl : AlertController,
              private actionSheetCtrl: ActionSheetController,
              private navParams: NavParams,
              private booking_service : Booking_Service,
              private modalCtrl: ModalController
            ) {

  }
  /*
  * it calls the select date/time screen
  */
  ionViewDidLoad(){

    this.platform.ready().then(() => {

    /*  this.line.id = this.navParams.get('lineid');
      this.line.nome = this.navParams.get('linename'); */
      if(this.navParams.get('success'))
      {
        this.date = this.navParams.get('date');
        this.travel = this.navParams.get('travel');
        this.showInfo = true;
      }else{
        this.show_message(MESSAGE_BOOKING_INFO_ERROR);
      }


      /*let modal = this.modalCtrl.create(Travel_Date_Time_Modal,{lineid: this.line.id});

      modal.onDidDismiss(data =>{
        if(data.success)
        {
          this.date = data.date;
          this.travel = data.travel;
          this.showInfo = true;
        }else{
            this.show_message(MESSAGE_BOOKING_INFO_ERROR);
        }

      });
      modal.present(); */
    });
  }

  /*
  * show all the error MESSAGES
  * @message is a message text to user
  */

  show_message(message)
  {
    this.alertCtrl.create({
      title : MESSAGE_ERROR,
      subTitle : message,
      buttons : [
        {
          text:'OK',
          handler: data =>{
            //this.get_cities();
            this.go_back_screen();
          }
    }]
    }).present();
  }

  /*
  * it goes back to the last screen
  */
  go_back_screen()
  {
    this.navCtrl.pop();
  }


}
