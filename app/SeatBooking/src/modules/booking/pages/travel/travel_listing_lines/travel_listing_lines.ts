import { Component } from '@angular/core';
import { NavController,LoadingController, Platform ,AlertController,ActionSheetController} from 'ionic-angular';

import {Booking_Service} from '../../../services/booking.service';

// importing models
import {Usuario} from '../../../../../models/Usuario';

//importing pages
import {List_reserves} from '../../../../list_reserves/pages/list_reserves/list_reserves';
import {Travel_Info} from '../travel_info/travel_info';
import {City} from '../../city/city';
import {Travel_Single_Booking} from '../travel_booking/travel_single_booking/travel_single_booking';
import {Travel_Periodic_Booking} from '../travel_booking/travel_periodic_booking/travel_periodic_booking';
import {Travel_Date_Time} from '../travel_date_time/travel_date_time';

// importing the message text
import {
        MESSAGE_BOOKING_NO_LINES,
        MESSAGE_REFRESH_PULL_TEXT,
        MESSAGE_REFRESH_REFRESHING_TEXT,
        MESSAGE_BOOKING_SEARCHING_LINES,
        MESSAGE_BOOKING_LINE_ACTION_INFO,
        MESSAGE_BOOKING_LINE_ACTION_BOOKING,
        MESSAGE_BOOKING_LINE_ACTION_BOOKING_PERIODIC,
        MESSAGE_BOOKING_LINE_ACTION_CANCEL,
        MESSAGE_HTTP_ERROR
} from '../../../../../messages';

@Component({
  selector: 'travel_listing_lines',
  templateUrl: 'travel_listing_lines.html'
})
export class Travel_Listing_Lines {
  /* MESSAGES */
  massage_refresh_pull_text = MESSAGE_REFRESH_PULL_TEXT;
  massage_refresh_refreshing_text = MESSAGE_REFRESH_REFRESHING_TEXT;
  message_no_travels = MESSAGE_BOOKING_NO_LINES;

  /* ATRIBUTES */
  cityid:any;
  cityname:any;
  lines:any;
  showlines:boolean = false;
  nolines:boolean = false;
  searchTerm: string = '';
  lines_filtered:any;
  refresher:any;

  constructor(public navCtrl: NavController,
              private platform: Platform,
              private loadingCtrl : LoadingController,
              private alertCtrl : AlertController,
              private booking_service : Booking_Service,
              private actionSheetCtrl: ActionSheetController
            ) {

  }

  ionViewDidLoad(){
    //this.latitude += "passou aqui";

    this.platform.ready().then(() => {

      this.cityid = Usuario.getInstance().get_cidade().get_id();
      this.cityname = Usuario.getInstance().get_cidade().get_nome();
      this.get_lines();

    });
  }

 /*
 * Get the bus lines
 */
  get_lines()
  {
    //this.noLines=true;
    //this.refresher.complete();
    let loadingMessage = this.loadingCtrl.create({content: MESSAGE_BOOKING_SEARCHING_LINES });
    loadingMessage.present();
    this.booking_service.getLines(this.cityid).then(
      data => {
          if(data != null && data != " ")
          {
            loadingMessage.dismiss();
            this.lines = data;
            this.lines_filtered = data;
            this.showlines = true;
            this.nolines = false;
          }else{
            loadingMessage.dismiss();
            this.showlines = false;
            this.nolines = true;
          }
      }
    ).catch(
      error => {
          loadingMessage.dismiss();
          this.show_message_erro(MESSAGE_HTTP_ERROR);
      }
    );
  }

 /*
  * show the actions to line selected
  * @line line object selected
 */
  line_selected(line){
    let actionSheet = this.actionSheetCtrl.create({
      title: line.nome,
      buttons: [
        {
          text: MESSAGE_BOOKING_LINE_ACTION_INFO,
          handler: () =>{
            this.change_screen("info",line);
          }
        },
        {
          text: MESSAGE_BOOKING_LINE_ACTION_BOOKING,
          handler: () =>{
            this.change_screen("booking",line);
          }
        },
        {
          text: MESSAGE_BOOKING_LINE_ACTION_BOOKING_PERIODIC,
          handler: () =>{
            this.change_screen("booking_periodic",line);
          }
        },
        {
          text: MESSAGE_BOOKING_LINE_ACTION_CANCEL,
          role: 'cancel'
        }
      ]
    }).present();

  }

  /*
  * refresh the page
  * @refresher refresher object
  */
   doRefresh(refresher){
     refresher.complete();
     this.get_lines();
   }

  /*
  *searchBar configuration
  * find the datas acording to the searchTerm
  * @searchTerm term filtered
  */
  filterItems(searchTerm){

         return this.lines.filter((item) => {
             return item.nome.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
         });

   }

   /*
   * called from interface when searchBar is used
   */
   setFilteredItems() {

         this.lines_filtered = this.filterItems(this.searchTerm);

  }

 /*
 * go to city page, where it can select other city
 */
  change_city()
  {
    this.navCtrl.push(City);
  }

  /*
  *it shows all messages and alerts to user
  * @message message which will be shown on the screen
  */
  show_message_erro(message)
  {
      this.alertCtrl.create({
        title : "Erro",
        subTitle : message,
        buttons : [
          {
            text:'OK',
            handler: data =>{
              //this.get_cities();
              this.go_back_screen();
            }
      }]
      }).present();

  }

 /*
 * change the screen acording to the action selected
 */
  change_screen(screen,line)
  {
    /*switch(screen){
      case "info" : this.navCtrl.push(Travel_Info,{lineid:line.idlinha,linename:line.nome}); break;
      case "booking" : this.navCtrl.push(Travel_Single_Booking,{lineid:line.idlinha,linename:line.nome}); break;
      case "booking_periodic" : this.navCtrl.push(Travel_Periodic_Booking,{lineid:line.idlinha,linename:line.nome}); break;

    } */
    let page;
    let isPeriodicBooking = false;
    switch(screen){
      case "info" : page = Travel_Info; break;
      case "booking" : page = Travel_Single_Booking;  break;
      case "booking_periodic" : page = Travel_Periodic_Booking; isPeriodicBooking = true; break;

    }

    this.navCtrl.push(Travel_Date_Time,{lineid:line.idlinha,linename:line.nome,nextpage: page, isPeriodicBooking : isPeriodicBooking});
  }

 /*
 * go back to list reserves module
 */
  go_back_screen()
  {
    this.navCtrl.setRoot(List_reserves);
  }

}
