import {Injectable, EventEmitter} from '@angular/core';
import {Http_Service} from '../../../services/http.service';
import {Geolocation_Service} from '../../../services/geolocation.service';

// importing models
import {Viagem} from '../../../models/Viagem';
import {Motorista} from '../../../models/Motorista';
import {Onibus} from '../../../models/Onibus';
import {Assento} from '../../../models/Assento';
import {Parada} from '../../../models/Parada';

@Injectable()
export class Booking_Service{

  constructor(private http_service:Http_Service, private geolocation_service : Geolocation_Service){}

  getLocation(){

    return new Promise((resolve,reject) => {
      this.geolocation_service.getCity().then( coords => {
        resolve(coords);
      }).catch((error) => {
        reject(null);
      });
    });
  }

  getCities(){
    return new Promise((resolve,reject) =>{
      let request = this.http_service.exec_get("/seatbooking/cities").subscribe(
        data => {
            if(data.success)
            {
              request.unsubscribe();
              resolve(data.cities);
            }else{
              request.unsubscribe();
              reject(null);
            }
      }, error => {
        request.unsubscribe();
        reject(null);
      });
    });

  }

  setCity(userCity){
    return new Promise((resolve,reject) =>{
      let request = this.http_service.exec_post("/user/city",userCity).subscribe(
        data => {
            if(data.success)
            {
              request.unsubscribe();
              resolve(true);
            }else{
              request.unsubscribe();
              reject(null);
            }
      }, error => {
        request.unsubscribe();
        reject(null);
      });
    });
  }

  getLines(cityId)
  {
    return new Promise((resolve,reject) => {
      let request = this.http_service.exec_get("/seatbooking/busLines/"+cityId).subscribe(
        data => {
          if(data.success)
          {
            request.unsubscribe();
            resolve(data.lines);
          }else{
            request.unsubscribe();
            reject(null)
          }
        }, error => {
          request.unsubscribe();
          reject(null);
        }
      );
    });
  }

  getTravels(lineId)
  {
    return new Promise((resolve,reject) => {
      let request = this.http_service.exec_get("/seatbooking/travels/"+lineId).subscribe(
        data => {
          if(data.success)
          {
            let travels = [];
            for(let travel of data.travels)
            {
              let travelObj = new Viagem(travel.idviagem,travel.nomelinha,travel.valor_horario_viagem_partida,travel.valor_horario_viagem_chegada);
              //travelObj.set_motorista(new Motorista(travel.idmotorista,travel.motorista_nome));
              //travelObj.set_onibus(new Onibus(travel.idonibus,travel.onibus_modelo,travel.onibus_marca));
              travels.push(travelObj);
            }
            request.unsubscribe();
            resolve(travels);
          }else{
            request.unsubscribe();
            reject(null)
          }
        }, error => {
            request.unsubscribe();
            reject(null);
          }
      );
    });
  }

  getTravel(travel:Viagem,date)
  {
    return new Promise((resolve,reject) =>{
      let request = this.http_service.exec_get("/seatbooking/travel/"+travel.get_id()+"/"+date).subscribe(
        data => {
            if(data.success)
            {
              //let info = data.travel_info;
              travel.set_motorista(new Motorista(data.travel_info[0].idmotorista,data.travel_info[0].motorista_nome));
              let onibus = new Onibus(data.travel_info[0].idonibus,data.travel_info[0].onibus_modelo,data.travel_info[0].onibus_marca);

              let assentos = [];
              for(let i=0; i < data.bus_seats.length;i++)
              {
                let assento = new Assento(data.bus_seats[i].idassento,data.bus_seats[i].preferencial,data.bus_seats[i].identificacao);
                if(data.bus_seats[i].reserva_data != null)
                {
                  assento.set_ocupado(true);
                }
                assentos.push(assento);
              }
              onibus.set_assentos(assentos);

              let paradas = [];
              for(let i=0; i < data.bus_stops.length;i++)
              {
                let parada = new Parada(data.bus_stops[i].idparada,data.bus_stops[i].endereco);
                paradas.push(parada);
              }

              travel.set_paradas(paradas);
              travel.set_onibus(onibus);
              request.unsubscribe();
              resolve(travel);
            }else{
              request.unsubscribe();
              reject(null);
            }
        },
        err => {
          request.unsubscribe();
          reject(null);
        }
      );
    });
  }

  createBooking(info){
    return new Promise((resolve,reject) =>{
      let request = this.http_service.exec_post("/seatbooking/booking",info).subscribe(
        data => {
            if(data.success)
            {
              request.unsubscribe();
              resolve(true);
            }else{
              request.unsubscribe();
              reject(data);
            }
      }, error => {
        request.unsubscribe();
        reject(null);
      });
    });
  }
  updateBooking(info)
  {
    return new Promise((resolve,reject) =>{
      let request = this.http_service.exec_put("/seatbooking/booking",info).subscribe(
        data => {
            if(data.success)
            {
              request.unsubscribe();
              resolve(true);
            }else{
              request.unsubscribe();
              reject(data);
            }
      }, error => {
        request.unsubscribe();
        reject(null);
      });
    });
  }
  createPeriodicBooking(info){
    return new Promise((resolve,reject) =>{
      let request = this.http_service.exec_post("/seatbooking/periodicBooking",info).subscribe(
        data => {
            if(data.success)
            {
              request.unsubscribe();
              resolve(true);
            }else{
              request.unsubscribe();
              reject(data);
            }
      }, error => {
        request.unsubscribe();
        reject(null);
      });
    });
  }
}
