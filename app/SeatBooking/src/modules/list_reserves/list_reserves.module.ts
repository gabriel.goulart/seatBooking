//import { CommonModule } from '@angular/common'; // usar quandofr criar um modulo
import {IonicPageModule} from 'ionic-angular';
import { NgModule } from '@angular/core';



import { List_reserves } from './pages/list_reserves/list_reserves';
import { List_periodic_reserves } from './pages/list_periodic_reserves/list_periodic_reserves';



@NgModule({
  declarations: [
   List_reserves,
   List_periodic_reserves
  ],
  imports: [
   IonicPageModule.forChild(List_reserves)
  ],
   entryComponents: [
    List_reserves,
    List_periodic_reserves
   ],
})

export class List_reserves_module { }
