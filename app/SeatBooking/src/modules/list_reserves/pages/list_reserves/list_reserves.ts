import { Component } from '@angular/core';
import { NavController,Platform,LoadingController,AlertController,ModalController,ToastController } from 'ionic-angular';

// importing services
import {List_Reserves_Service} from '../../services/list_reserves.service';

// importing models
import {Usuario} from "../../../../models/Usuario";
import {Reserva} from "../../../../models/Reserva";
import {ReservaUnica} from "../../../../models/ReservaUnica";
import {ReservaPeriodica} from "../../../../models/ReservaPeriodica";

//importing pages
import {Travel_Single_Booking} from "../../../booking/pages/travel/travel_booking/travel_single_booking/travel_single_booking";
import {List_periodic_reserves} from '../list_periodic_reserves/list_periodic_reserves';
// importing the message text
import {
        TITLE_PAGE_LIST_BOOKINGS,
        TEXT_LIST_BOOKINGS_SEGMENT_SINGLE,
        TEXT_LIST_BOOKINGS_SEGMENT_PERIODIC,
        TEXT_LIST_BOOKINGS_SEGMENT_PERIODIC_INITIAL_DATE,
        TEXT_LIST_BOOKINGS_SEGMENT_PERIODIC_FINAL_DATE,
        TEXT_LIST_BOOKINGS_SEGMENT_SINGLE_DATE,
        TEXT_LIST_BOOKINGS_SEGMENT_SINGLE_SEAT,
        TEXT_LIST_BOOKINGS_BUTTON_EDIT,
        TEXT_LIST_BOOKINGS_BUTTON_EXCLUDE,
        TEXT_LIST_BOOKINGS_TRAVEL_INFO_DEPARTURE_TIME,
        TEXT_LIST_BOOKINGS_TRAVEL_INFO_ARRIVAL_TIME,
        MESSAGE_REFRESH_PULL_TEXT,
        MESSAGE_REFRESH_REFRESHING_TEXT,
        MESSAGE_LIST_BOOKINGS_DELETING_BOOKING,
        MESSAGE_LIST_BOOKINGS_GET_BOOKINGS,
        MESSAGE_LIST_BOOKINGS_GETTING_TRAVEL_INFO,
        MESSAGE_LIST_BOOKINGS_DELETED_BOOKING,
        MESSAGE_YES,
        MESSAGE_NO,
        MESSAGE_ERROR,
        MESSAGE_HTTP_ERROR
} from '../../../../messages';

@Component({
  selector: 'list_reserves',
  templateUrl: 'list_reserves.html'
})
export class List_reserves {
 title = TITLE_PAGE_LIST_BOOKINGS;
 textsinglebookingsegment =TEXT_LIST_BOOKINGS_SEGMENT_SINGLE;
 textperiodicbookingsegment = TEXT_LIST_BOOKINGS_SEGMENT_PERIODIC;
 textperiodicinitialdate =TEXT_LIST_BOOKINGS_SEGMENT_PERIODIC_INITIAL_DATE;
 textperiodicfinaldate =TEXT_LIST_BOOKINGS_SEGMENT_PERIODIC_FINAL_DATE;
 textsingledate = TEXT_LIST_BOOKINGS_SEGMENT_SINGLE_DATE;
 textsinglseat=TEXT_LIST_BOOKINGS_SEGMENT_SINGLE_SEAT;
 textbuttonedit=TEXT_LIST_BOOKINGS_BUTTON_EDIT;
 textbuttonexclude= TEXT_LIST_BOOKINGS_BUTTON_EXCLUDE;
 textdeparturetime = TEXT_LIST_BOOKINGS_TRAVEL_INFO_DEPARTURE_TIME;
 textarrivaltime = TEXT_LIST_BOOKINGS_TRAVEL_INFO_ARRIVAL_TIME;
 massage_refresh_pull_text = MESSAGE_REFRESH_PULL_TEXT;
 massage_refresh_refreshing_text = MESSAGE_REFRESH_REFRESHING_TEXT;

 segment:string = "single";
 listSingleBooking:Reserva[] = [];
 listPeriodicBooking:Reserva[] = [];


  constructor(public navCtrl: NavController,
              private platform: Platform,
              private loadingCtrl : LoadingController,
              private alertCtrl : AlertController,
              private modalCtrl: ModalController,
              private toastCtrl: ToastController,
              private list_reserves_service : List_Reserves_Service
              ) {}

  ionViewDidLoad(){
    this.platform.ready().then(() => {
      this.getBookings();
    });
  }

  getBookings()
  {
      let loadingMessage = this.loadingCtrl.create({content: MESSAGE_LIST_BOOKINGS_GET_BOOKINGS });
      loadingMessage.present();

      this.list_reserves_service.getAllBookings(Usuario.getInstance().get_id()).then(
        data =>{
           if(data)
           {
             this.listSingleBooking = Usuario.getInstance().get_reservas_unica();
             this.listPeriodicBooking = Usuario.getInstance().get_reservas_periodica();
             loadingMessage.dismiss();
           }else{
             loadingMessage.dismiss();
             this.show_message(MESSAGE_HTTP_ERROR,true);
           }

      }).catch(
        error => {
          loadingMessage.dismiss();
          this.show_message(MESSAGE_HTTP_ERROR,true);
      });
  }

  deleteSingleBooking(index,booking)
  {
    let loadingMessage = this.loadingCtrl.create({content: MESSAGE_LIST_BOOKINGS_DELETING_BOOKING });
    loadingMessage.present();

    this.list_reserves_service.deleteSingleBooking(booking.get_data(),booking.get_viagem().get_id(),Usuario.getInstance().get_id()).then(
      data =>{
         if(data)
         {

           loadingMessage.dismiss();
           this.listSingleBooking.splice(index,1);
           this.show_message(MESSAGE_LIST_BOOKINGS_DELETED_BOOKING,false);
         }else{
           loadingMessage.dismiss();
           this.show_message(MESSAGE_HTTP_ERROR,true);
         }

    }).catch(
      error => {
        loadingMessage.dismiss();
        this.show_message(MESSAGE_HTTP_ERROR,true);
    });
  }

  editSigleBooking(index,booking)
  {
    let loadingMessage = this.loadingCtrl.create({content: MESSAGE_LIST_BOOKINGS_GETTING_TRAVEL_INFO });
    loadingMessage.present();

    this.list_reserves_service.getTravel(booking.get_viagem(),booking.get_data()).then(
      data =>{
         if(data != null)
         {
           booking.set_viagem(data);
           loadingMessage.dismiss();
           this.open_single_booking_modal(index,booking);
         }else{
           loadingMessage.dismiss();
           this.show_message(MESSAGE_HTTP_ERROR,true);
         }

    }).catch(
      error => {
        loadingMessage.dismiss();
        this.show_message(MESSAGE_HTTP_ERROR,true);
    });
  }

  open_single_booking_modal(index,booking)
  {

    let modal = this.modalCtrl.create(Travel_Single_Booking,{success:true,date:booking.get_data(),travel:booking.get_viagem(),updateSeat:true});

    modal.onDidDismiss(data =>{
      if(data.success)
      {

        this.getBookings();

      }

    });
    modal.present();
  }

  deletePeriodicBooking(index,periodicBooking)
  {

    let loadingMessage = this.loadingCtrl.create({content: MESSAGE_LIST_BOOKINGS_DELETING_BOOKING });
    loadingMessage.present();

    this.list_reserves_service.deletePeriodicBooking(periodicBooking.get_id()).then(
      data =>{
         if(data)
         {

           loadingMessage.dismiss();
           this.listPeriodicBooking.splice(index,1);
           this.show_message(MESSAGE_LIST_BOOKINGS_DELETED_BOOKING,false);

         }else{
           loadingMessage.dismiss();
           this.show_message(MESSAGE_HTTP_ERROR,true);
         }

    }).catch(
      error => {
        loadingMessage.dismiss();
        this.show_message(MESSAGE_HTTP_ERROR,true);
    });
  }
  editPeriodicBooking(periodicBooking)
  {
      this.navCtrl.push(List_periodic_reserves,{periodicBooking : periodicBooking});

  }

  /* refresh the page*/
  doRefresh(refresher){
    refresher.complete();
    this.getBookings();
  }
  /* it shows all messages and alerts to user*/
  show_message(message,error:boolean)
  {
    if(error){
      this.alertCtrl.create({
        title : MESSAGE_ERROR,
        subTitle : message,
        buttons : [
          {
            text:'OK',
            handler: data =>{

           // this.getBookings();
            }
      }]
      }).present();
    }else{
      this.toastCtrl.create(
       {
         message : message,
         duration: 5000,
         position: "top"
       }
      ).present();
    }
  }

}
