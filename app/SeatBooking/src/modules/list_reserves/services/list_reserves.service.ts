import {Injectable, EventEmitter} from '@angular/core';
import {Http_Service} from '../../../services/http.service';
import {Geolocation_Service} from '../../../services/geolocation.service';

// importing models
import {Usuario} from '../../../models/Usuario';
import {Viagem} from '../../../models/Viagem';
import {Motorista} from '../../../models/Motorista';
import {Onibus} from '../../../models/Onibus';
import {Assento} from '../../../models/Assento';
import {Parada} from '../../../models/Parada';
import {Reserva} from '../../../models/Reserva';
import {ReservaUnica} from '../../../models/ReservaUnica';
import {ReservaPeriodica} from '../../../models/ReservaPeriodica';

@Injectable()
export class List_Reserves_Service{

  constructor(private http_service:Http_Service){

  }

  getAllBookings(userId)
  {
    return new Promise((resolve,reject) =>{
      let request = this.http_service.exec_get("/seatbooking/allBookings/"+userId).subscribe(
        data => {
            if(data.success)
            {
              let singleBooking: Reserva[] = [];
              for(let i=0; i < data.single.length;i++)
              {
                let seat = new Assento(data.single[i].idassento,data.single[i].assento_preferencial,data.single[i].assento_identificacao);
                let travel = new Viagem(data.single[i].idviagem,data.single[i].viagem_linha,data.single[i].horario_partida,data.single[i].horario_chegada);
                let booking : Reserva = new ReservaUnica(data.single[i].data,travel,seat);
                singleBooking.push(booking);
              }

              let periodicBooking: Reserva[] = [];
              for(let i=0; i < data.periodic.length;i++)
              {
                let datas = [data.periodic[i].data_inicio,data.periodic[i].data_fim];
                let travel_periodic = new Viagem(data.periodic[i].idviagem,data.periodic[i].viagem_linha,data.periodic[i].horario_partida,data.periodic[i].horario_chegada);
                let booking_periodic : Reserva = new ReservaPeriodica(data.periodic[i].idreserva_periodica,datas,travel_periodic);
                periodicBooking.push(booking_periodic);
              }

              Usuario.getInstance().set_reservas_unica(singleBooking);
              Usuario.getInstance().set_reservas_periodica(periodicBooking);
              request.unsubscribe();
              resolve(true);
            }else{
              request.unsubscribe();
              reject(false);
            }
      }, error => {
        request.unsubscribe();
        reject(false);
      });
    });
  }

  deleteSingleBooking(bookingDate,bookingTravel,iduser)
  {
    return new Promise((resolve,reject) => {
      let request = this.http_service.exec_delete("/seatbooking/booking/"+bookingDate+"/"+bookingTravel+"/"+iduser).subscribe(
        data => {
            if(data.success)
            {
              resolve(true);
            }else{
              reject(false);
            }
        },
        err => {
          reject(false);
        }
      );
    });
  }

  deletePeriodicBooking(periodicBookingId)
  {
    return new Promise((resolve,reject) => {
      let request = this.http_service.exec_delete("/seatbooking/periodicBooking/"+periodicBookingId).subscribe(
        data => {
            if(data.success)
            {
              resolve(true);
            }else{
              reject(false);
            }
        },
        err => {
          reject(false);
        }
      );
    });
  }

  getBookingsByPeriodicId(periodicBooking)
  {
    return new Promise((resolve,reject) => {
      let request = this.http_service.exec_get("/seatbooking/bookingsByPeriodicId/"+periodicBooking.get_id()).subscribe(
        data => {
            if(data.success)
            {
              let bookings:Reserva[] = [];
              for(let booking of data.bookings)
              {
                let seat = new Assento(booking.idassento,booking.assento_preferencial,booking.assento_identificacao);
                let travel = new Viagem(booking.idviagem,booking.viagem_linha,booking.horario_partida,booking.horario_chegada);
                let newBooking : Reserva = new ReservaUnica(booking.data,travel,seat);
                bookings.push(newBooking);
              }
              periodicBooking.set_reservas(bookings);
              resolve(periodicBooking);
            }else{
              reject(null);
            }
        },
        err => {
          reject(null);
        }
      );
    });
  }

  getTravel(travel:Viagem,date)
  {
    return new Promise((resolve,reject) =>{
      let request = this.http_service.exec_get("/seatbooking/travel/"+travel.get_id()+"/"+date).subscribe(
        data => {
            if(data.success)
            {
              travel.set_motorista(new Motorista(data.travel_info[0].idmotorista,data.travel_info[0].motorista_nome));
              let onibus = new Onibus(data.travel_info[0].idonibus,data.travel_info[0].onibus_modelo,data.travel_info[0].onibus_marca);

              let assentos = [];
              for(let i=0; i < data.bus_seats.length;i++)
              {
                let assento = new Assento(data.bus_seats[i].idassento,data.bus_seats[i].preferencial,data.bus_seats[i].identificacao);
                if(data.bus_seats[i].reserva_data != null)
                {
                  assento.set_ocupado(true);
                }
                assentos.push(assento);
              }
              onibus.set_assentos(assentos);

              let paradas = [];
              for(let i=0; i < data.bus_stops.length;i++)
              {
                let parada = new Parada(data.bus_stops[i].idparada,data.bus_stops[i].endereco);
                paradas.push(parada);
              }

              travel.set_paradas(paradas);
              travel.set_onibus(onibus);
              request.unsubscribe();
              resolve(travel);
            }else{
              request.unsubscribe();
              reject(null);
            }
        },
        err => {
          request.unsubscribe();
          reject(null);
        }
      );
    });
  }
}
