import {Injectable} from '@angular/core';
import { Geolocation} from '@ionic-native/geolocation';
import {Http_Service} from './http.service';

@Injectable()
export class Geolocation_Service{
  constructor(private geolocation : Geolocation, private http_service : Http_Service) {

  }

  getLocation():Promise<any>
  {

    return new Promise((resolve,reject) => {
      let options = { enableHighAccuracy : true };
      this.geolocation.getCurrentPosition(options).then( (resp ) => {
        resolve(resp.coords);

      }).catch((error) => {
        reject(error);
      });
    });

  }

  getCity()
  {
      return new Promise((resolve,reject) => {
        this.getLocation().then(coords =>{
            let request = this.http_service.exec_get("/seatbooking/location/"+coords.latitude+"/"+coords.longitude).subscribe(
              data => {

                if(data.success)
                {
                  request.unsubscribe();
                  resolve(data.city);
                }else{
                  request.unsubscribe();
                  reject(null);
                }

              },
              err =>{
                request.unsubscribe();
                reject(null);
              }
            );

        }).catch(error => {
              reject(null);
        });
      });
  }
}
