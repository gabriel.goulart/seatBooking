import {Injectable} from '@angular/core';
import {Http,Headers} from '@angular/http';
//import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
//import 'rxjs/add/observable/forkJoin';
import {API_URL,HTTP_TIMEOUT} from '../Config';
import {Usuario} from '../models/Usuario';
@Injectable()
export class Http_Service{

  constructor(public http:Http){

  }

  exec_get(url:string){
    let headers = new Headers();
    if(Usuario.getInstance().get_token() != null && Usuario.getInstance().get_token() != " "){
      headers.append('Authorization',Usuario.getInstance().get_token());
    }
    return this.http.get(API_URL+url,{headers: headers}).timeout(HTTP_TIMEOUT).map(res => res.json());

  }

  exec_post(url:string,data, header_type='application/json')
  {
    let headers = new Headers();
    if(Usuario.getInstance().get_token() != null && Usuario.getInstance().get_token() != " "){
      headers.append('Authorization',Usuario.getInstance().get_token());
    }
    headers.append('Content-Type',header_type);

    return this.http.post(API_URL+url,JSON.stringify(data),{headers: headers}).timeout(HTTP_TIMEOUT).map(res => res.json());

  }

  exec_delete(url:string)
  {
    let headers = new Headers();
    if(Usuario.getInstance().get_token() != null && Usuario.getInstance().get_token() != " "){
      headers.append('Authorization',Usuario.getInstance().get_token());
    }
    return this.http.delete(API_URL+url,{headers: headers}).timeout(HTTP_TIMEOUT).map(res => res.json());
  }

  exec_put(url:string,data, header_type='application/json')
  {
    let headers = new Headers();
    if(Usuario.getInstance().get_token() != null && Usuario.getInstance().get_token() != " "){
      headers.append('Authorization',Usuario.getInstance().get_token());
    }
    headers.append('Content-Type',header_type);

    return this.http.put(API_URL+url,JSON.stringify(data),{headers: headers}).timeout(HTTP_TIMEOUT).map(res => res.json());

  }


}
