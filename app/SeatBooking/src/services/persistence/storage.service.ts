import {Injectable} from '@angular/core';
import { Storage } from '@ionic/storage';



@Injectable()
export class Storage_Service{

  constructor(private storage: Storage){

  }


  public get(key) : Promise<any>
  {
    return this.storage.get(key);
  }

  public set(key,value)
  {
    this.storage.set(key,value).then((val) => {

    }).catch(() => {

    });
  }


}
