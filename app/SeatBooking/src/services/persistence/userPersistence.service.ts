import {Injectable} from '@angular/core';
import {Storage_Service} from './storage.service';
import { Usuario } from '../../models/Usuario';
import { Cidade } from '../../models/Cidade';



@Injectable()
export class UserPersistence_Service{

  constructor(private storage_service:Storage_Service){

  }

  save()
  {
    this.storage_service.set("usuario",JSON.stringify(Usuario.getInstance().serialize()));
  }

  get()
  {
    let usuario_id = null;let usuario_nome = null; let usuario_idufsc = null; let usuario_email = null;let usuario_token = null;

    return new Promise( (resolve, reject) => {
      this.storage_service.get("usuario").then(val =>{
        if(val != null)
        {
          let bd_user = JSON.parse(val);
          usuario_id = bd_user.id;usuario_nome=bd_user.nome;usuario_idufsc=bd_user.idufsc;usuario_email=bd_user.email;usuario_token=bd_user.token;
          if(usuario_id != null && usuario_nome != null && usuario_email != null && usuario_email != null && usuario_token != null)
          {
            Usuario.getInstance().set_id(usuario_id);
            Usuario.getInstance().set_nome(usuario_nome);
            Usuario.getInstance().set_idufsc(usuario_idufsc);
            Usuario.getInstance().set_email(usuario_email);
            Usuario.getInstance().set_token(usuario_token);

            if(bd_user.cidadeNome != null && bd_user.cidadeId != null)
            {
              let cidade = new Cidade(bd_user.cidadeId,bd_user.cidadeNome);
              Usuario.getInstance().set_cidade(cidade);
            }
            //this.storage_service.get("").then().catch();
            resolve(true);
          }else{
            reject(false);
          }
        }else{
          reject(false);
        }

      }).catch(erro =>{
        reject(false);
      });
    });

  }

  clean()
  {
    this.storage_service.set("usuario",null);
  }
}
